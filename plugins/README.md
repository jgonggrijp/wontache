# Wontache integrations

*Integrations for Wontache with other libraries and tools*

This is a directory with public [workspaces][workspaces] within the [Wontache][wontache] project. It provides Wontache integrations for other projects. See the README within each subdirectory for further information.

[wontache]: https://gitlab.com/jgonggrijp/wontache
[workspaces]: https://classic.yarnpkg.com/en/docs/workspaces
