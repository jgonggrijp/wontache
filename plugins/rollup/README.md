[![latest version on npm][shield-npm]][npm] [![author: Julian Gonggrijp][shield-jgonggrijp]][jgonggrijp] [![license text][shield-license]][license] [![code hosted on GitLab][shield-gitlab]][repo] [![changelog][shield-versions]][versions] [![issue tracker on GitLab][shield-issues]][tracker] [![pipeline status][shield-pipeline]][build]

[repo]: https://gitlab.com/jgonggrijp/wontache
[tracker]: https://gitlab.com/jgonggrijp/wontache/issues
[versions]: https://gitlab.com/jgonggrijp/wontache/-/releases
[license]: https://gitlab.com/jgonggrijp/wontache/blob/master/LICENSE
[build]: https://gitlab.com/jgonggrijp/wontache/-/commits/integration
[npm]: https://www.npmjs.com/package/rollup-plugin-wontache
[jgonggrijp]: https://juliangonggrijp.com

# rollup-plugin-wontache

*[Rollup][rollup] plugin for bundling [Mustache][mustache] templates with the [Wontache][wontache] engine*

[rollup]: https://rollupjs.org
[mustache]: http://mustache.github.io
[wontache]: https://jgonggrijp.gitlab.io/wontache/index.html


## Quickstart

Your template (`hello.mustache`):

```mustache

Hello, {{name}}!
```

Your code, which uses the template (`index.js`):

```js

import hello from './hello.mustache';
console.log(hello({name: 'World'}));
```

Your `rollup.config.js`:

```js

import wontache from 'rollup-plugin-wontache';

export default {
    input: './index.js',
    plugins: [wontache()],
    // ...
};
```


## Options

```js

import wontache from 'rollup-plugin-wontache';

// ...
    wontache({
        include: '**/*.mustache',
        exclude: [],
        precompile: false,
        delimiters: ['{{', '}}'],
        wontache: 'mustache',
    })
```


### include

*Glob string, or array of glob strings* &mdash; **Default:** `'**/*.mustache'`

Transform only modules of which the file name matches the given pattern(s). Patterns are matched against the absolute path on disk. An empty array will include everything.


### exclude

*Glob string, or array of glob strings* &mdash; **Default:** `[]`

Skip modules of which the file name matches the given pattern(s). Patterns are matched against the absolute path on disk. An empty array will skip nothing.


### precompile

*Boolean* &mdash; **Default:** `false`

This option only controls an optimization. The transformed module will always export a compiled, ready-to-run template function, regardless of whether you pass `true` or `false`.

If `false`, the compilation happens during initial loading of the transformed module. If `true`, the compilation already happens while Rollup is performing the transform, so the transformed module does not need to perform this work anymore.

The optimization is off by default, because it also has a cost: the precompiled version of a template is always larger than the original template text. Leave it off to keep your bundle as small as possible. Try switching it on when you find that there is too much delay during initial loading of the bundle. If you are publishing a library that includes bundled templates, consider giving your users a choice between bundles with and without precompilation.

If you want the transformed module to export the template as a string, instead of as an already compiled function, use [`rollup-plugin-string`][plugin-string] instead.

[plugin-string]: https://github.com/TrySound/rollup-plugin-string


### delimiters

*Array of exactly two nonempty strings* &mdash; **Default:** `['{{', '}}']`

Override this to change the [delimiters][delimiters] with which your templates will be parsed initially, until encountering [Set Delimiter tags][set-delimiter].

[delimiters]: https://jgonggrijp.gitlab.io/wontache/index.html#changing-the-delimiters
[set-delimiter]: https://jgonggrijp.gitlab.io/wontache/mustache.5.html#Set-Delimiter

Your template with alternative delimiters:

```mustache

Hello, [name]!
```

Your override in `rollup.config.js`:

```js

// somewhere in an input config
    plugins: [
        wontache({
            delimiters: ['[', ']'],
        }),
    ],
```


### wontache

*String with valid JavaScript variable name* &mdash; **Default:** `'mustache'`

The transformed module imports the Mustache engine from Wontache. The `wontache` option lets you override the name by which it is imported.

Transformed module, as the plugin outputs it by default:

```js

import mustache from 'wontache';
export default mustache(...);
```

Your override in `rollup.config.js`:

```js

// somewhere in an input config
    plugins: [
        wontache({
            wontache: 'sideburns',
        }),
    ],
```

Alternative transformed module:

```js

import sideburns from 'wontache';
export default sideburns(...);
```


[shield-pipeline]: https://img.shields.io/gitlab/pipeline-status/jgonggrijp/wontache?branch=integration&label=pipeline
[shield-npm]: https://img.shields.io/npm/v/rollup-plugin-wontache
[shield-gitlab]: https://img.shields.io/badge/-GitLab-555?logo=gitlab
[shield-versions]: https://img.shields.io/badge/-change_log-555?logo=gitlab
[shield-issues]: https://img.shields.io/badge/-issues-555?logo=gitlab
[shield-jgonggrijp]: https://img.shields.io/badge/author-Julian_Gonggrijp-708
[shield-license]: https://img.shields.io/npm/l/rollup-plugin-wontache
