import { defaults } from 'underscore'
import wrapModule from 'wontache/wrap-module'
import { createFilter } from '@rollup/pluginutils'

# Our Rollup transform plugin is a very thin wrapper around `wrapModule`, adding
# only Rollup's conventional `include` and `exclude` options.
export default (options = {}) ->
    # We force the module type to `'ESM'`, since this is Rollup's foundation.
    # Also, we default the `include` option to files with a `'mustache'`
    # extension, to avoid transforming *everything*.
    options = defaults {type: 'ESM'}, options, {include: '**/*.mustache'}
    filter = createFilter options.include, options.exclude
    name: 'wontache'
    transform: (code, id) ->
        return unless filter id
        wrapModule code, options
