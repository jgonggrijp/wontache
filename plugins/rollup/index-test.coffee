import assert from 'assert'
import * as fs from 'fs'

import { times, map } from 'underscore'
import wrapModule from 'wontache/wrap-module'
import { rollup } from 'rollup'

import {
    sampleFile,
    noTemplateFile,
    randomOptions,
    ourPattern,
    commentPattern,
} from './test-util.coffee'
import wontache from './index.coffee'

# Contents of the two sample files that will be fed to Rollup.
sample = fs.readFileSync sampleFile, 'utf-8'
noTemplate = fs.readFileSync noTemplateFile, 'utf-8'

# Every time we run a Rollup bundle with our plugin, the input configuration
# will look as follows.
inputConfig = (entry, options) ->
    input: entry
    # We keep everything external, since we are only interested in the way the
    # template itself is rendered.
    external: /./
    plugins: [
        wontache options
    ]

# When finishing the bundle, Rollup reformats compared to `ourPattern`.
rollupPattern = /^(import .+)\n\nvar (\w+) = ([^]+?)\n\nexport default \2;/

# Do `actual` and `expected` represent the same code, despite some notational
# differences?
sameCode = (actual, expected) ->
    ourMatch = expected.match ourPattern
    rollupMatch = actual.match rollupPattern
    ourMatch[1] is rollupMatch[1] and ourMatch[2] is rollupMatch[3]

# Feed `file` to Rollup with our plugin and `options`. Feed `content` directly
# to `wrapModule` with the same `options`, and assert that both paths produce
# the same code.
assertTransforms = (file, content, options) ->
    expected = wrapModule content, options
    bundle = await rollup inputConfig file, options
    actual = await bundle.generate {}
    assert sameCode actual.output[0].code, expected

# Feed `file` to Rollup with our plugin and `options`. Assert that it reproduces
# `content` unmodified, modulo comments.
assertPasses = (file, expected, options) ->
    bundle = await rollup inputConfig file, options
    actual = await bundle.generate {}
    assert actual.output[0].code is expected.replace commentPattern, ''

# Oh by the way, this is a test module.
describe 'rollup plugin', ->
    it 'passes options straight to wrapModule', ->
        # A light fuzz test for starters.
        await Promise.all times 3, ->
            assertTransforms sampleFile, sample, randomOptions()

    it 'accepts sources based on the include option', ->
        assertTransforms sampleFile, sample, include: '**/*.mustache'

    it 'rejects sources based on the include option', ->
        assertPasses noTemplateFile, noTemplate, include: '**/*.mustache'

    it 'accepts sources based on the exclude option', ->
        assertTransforms sampleFile, sample, exclude: '**/*.js'

    it 'rejects sources based on the exclude option', ->
        assertPasses noTemplateFile, noTemplate, exclude: '**/*.js'

    it 'forces the module type to ESM', ->
        options = randomOptions()
        expected = wrapModule sample, options
        alternativeTypes = ['AMD', 'CommonJS', 'nonsense', '', true]
        await Promise.all map alternativeTypes, (type) ->
            options.type = type
            bundle = await rollup inputConfig sampleFile, options
            actual = await bundle.generate {}
            assert sameCode actual.output[0].code, expected
