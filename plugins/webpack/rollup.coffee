import coffee from 'rollup-plugin-coffee-script'
import { babel } from '@rollup/plugin-babel'
import { bundleInput } from 'wontache-devtools-rollup'

bundleInput
    input: 'index.coffee'
    external: /./
    plugins: [
        coffee()
        babel
            babelHelpers: 'bundled'
            extensions: ['.coffee', '.js']
            presets: [['@babel/preset-env',
                forceAllTransforms: true
            ]]
    ]
    output:
        file: 'index.js'
        format: 'cjs'
        exports: 'auto'
        sourcemap: true
.catch console.error
