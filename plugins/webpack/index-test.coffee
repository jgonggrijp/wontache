import assert from 'assert'
import path from 'path'
import * as fs from 'fs'

import { times, isArray } from 'underscore'
import wrapModule from 'wontache/wrap-module'
import webpack from 'webpack'
import { createFsFromVolume, Volume } from 'memfs'

import {
    sampleFile,
    randomOptions,
} from '../rollup/test-util'

sample = fs.readFileSync sampleFile, 'utf-8'

# Essential parts of the ajv validation error messages.
errorMessages =
    type: /options\.type should be one of[^]+ESM.+AMD.+CommonJS/
    precompile: /options\.precompile should be a boolean/
    delimiters:
        arity: /options\.delimiters should not have fewer than 2/
        type: /options\.delimiters\[1\] should be a string/
    wontache: /options\.wontache should match pattern/

# Try loading the sample file with given options.
# This function was imitated from the examples in the webpack documentation:
# https://webpack.js.org/contribute/writing-a-loader/#testing
load = (options = {}) ->
    compiler = webpack
        context: __dirname
        entry: sampleFile
        mode: 'none'
        output:
            path: path.resolve __dirname
            filename: 'sample.js'
        module:
            rules: [
                test: /\.mustache/
                use:
                    loader: path.resolve __dirname, 'index.js'
                    options: options
            ]
    compiler.outputFileSystem = createFsFromVolume new Volume
    compiler.outputFileSystem.join = path.join.bind path
    new Promise (resolve, reject) ->
        compiler.run (err, stats) -> switch
            when err then reject err
            when stats.hasErrors() then reject stats.toJson().errors
            else resolve stats

# Transpile the sample file with our loader and `options`. Feed its content
# directly to `wrapModule` with the same `options`, and assert that both paths
# produce the same code.
assertTransforms = (options) ->
    expected = wrapModule sample, options
    stats = await load options
    actual = stats.toJson(source: true).modules[0].source
    assert actual is expected

# Common routine for testing validation.
assertErrors = (options, expected) ->
    try
        await load options
        passed = true
    catch errors
        assert isArray errors
        assert errors.length is 1
        for expectation in expected
            assert expectation.test errors[0].message ? errors[0]
    finally
        assert not passed

# One would almost forget that this is a test module.
describe 'webpack loader', ->
    it 'passes options straight to wrapModule', ->
        # A light fuzz test for starters.
        Promise.all times 3, ->
            assertTransforms randomOptions()

    it 'validates the options', ->
        Promise.all [
            assertErrors {type: 'yellow'}, [errorMessages.type]
            assertErrors {precompile: 3, delimiters: ['{{}}']}, [
                errorMessages.precompile
                errorMessages.delimiters.arity
            ]
            assertErrors {delimiters: ['{{', null], wontache: '++x'}, [
                errorMessages.delimiters.type
                errorMessages.wontache
            ]
        ]
