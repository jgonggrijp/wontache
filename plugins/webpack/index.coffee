import wrapModule from 'wontache/wrap-module'
import { getOptions } from 'loader-utils'
import { validate } from 'schema-utils'

schema =
    type: 'object'
    properties:
        type:
            enum: ['ESM', 'AMD', 'CommonJS']
        precompile:
            type: 'boolean'
        delimiters:
            type: 'array'
            items: type: 'string'
            minItems: 2
            maxItems: 2
        wontache:
            type: 'string'
            pattern: '^[_A-Za-z][_A-Za-z0-9]*$'

# Our webpack loader is a very thin wrapper around `wrapModule`.
export default (code) ->
    options = @getOptions?() ? getOptions @
    validate schema, options,
        name: 'wontache'
        baseDataPath: 'options'
    wrapModule code, options
