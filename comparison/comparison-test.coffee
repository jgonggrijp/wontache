import assert from 'assert'
import { readFile } from 'fs'
import { resolve } from 'path'

import { times, constant } from 'underscore'

import * as jitLo from './build/jit.lodash.cjs'
import * as jit_ from './build/jit.underscore.cjs'
import * as jitWo from './build/jit.wontache.cjs'
import * as jitHo from './build/jit.hogan.cjs'
import * as jitMu from './build/jit.mustache.cjs'
import * as jitHa from './build/jit.handlebars.cjs'
import * as aotLo from './build/aot.lodash.cjs'
import * as aot_ from './build/aot.underscore.cjs'
import * as aotWo from './build/aot.wontache.cjs'
import * as aotHo from './build/aot.hogan.cjs'
import * as aotHa from './build/aot.handlebars.cjs'

here = __dirname

exampleData = new Promise (resolvePromise, rejectPromise) ->
    readFile resolve(here, './example-data.json'), 'utf-8', (error, result) ->
        if error then rejectPromise error else resolvePromise JSON.parse result

expectedOutput = new Promise (resolvePromise, rejectPromise) ->
    readFile resolve(here, './expected-output.html'), 'utf-8', (err, result) ->
        if err then rejectPromise err else resolvePromise result

spaceAroundTags = /(?<=>)\s+|\s+(?=<)/g
anyWhite = /\s+/g
normalizeWhite = (string) ->
    string.replace spaceAroundTags, ''
    .replace anyWhite, ' '

jitEngines =
    lodash: jitLo
    underscore: jit_
    wontache: jitWo
    'Hogan.js': jitHo
    'Mustache.js': jitMu
    handlebars: jitHa

aotEngines =
    lodash: aotLo
    underscore: aot_
    wontache: aotWo
    'Hogan.js': aotHo
    handlebars: aotHa

describe 'engine comparison', ->
    before ->
        [@exampleData, @rawExpectedOutput] = await Promise.all [exampleData, expectedOutput]
        @expectedOutput = normalizeWhite @rawExpectedOutput

    describe 'HTML whitespace normalization', ->
        before ->
            @rawNoWhite = @rawExpectedOutput.replace anyWhite, ''
            @noWhite = @expectedOutput.replace anyWhite, ''

        it 'is effective', ->
            assert not spaceAroundTags.test @expectedOutput
            assert @expectedOutput.length / @rawExpectedOutput.length < 0.9

        it 'only strips whitespace around tags', ->
            assert @rawNoWhite is @noWhite
            assert @rawNoWhite isnt @expectedOutput
            assert @rawNoWhite.length / @expectedOutput.length < 0.97

    describe 'JIT-compiling bundles', ->
        for name, engine of jitEngines
            do (name, engine) ->
                it name, ->
                    engine.compile()
                    result = normalizeWhite engine.run @exampleData
                    assert result is @expectedOutput
                    repeatedExamples = times 3, constant @exampleData
                    for result in engine.runEach repeatedExamples
                        assert normalizeWhite(result) is @expectedOutput

    describe 'AOT-compiled bundles', ->
        for name, engine of aotEngines
            do (name, engine) ->
                it name, ->
                    result = normalizeWhite engine.run @exampleData
                    assert result is @expectedOutput
                    repeatedExamples = times 3, constant @exampleData
                    for result in engine.runEach repeatedExamples
                        assert normalizeWhite(result) is @expectedOutput
