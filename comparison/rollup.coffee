import { each } from 'async'
import { string } from 'rollup-plugin-string'
import coffee from 'rollup-plugin-coffee-script'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { babel } from '@rollup/plugin-babel'
import { terser } from 'rollup-plugin-terser'
import { bundleInput } from 'wontache-devtools-rollup'

import lodashPlugin from './rollup-plugins/lodash.coffee'
import underscorePlugin from './rollup-plugins/underscore.coffee'
import wontachePlugin from 'rollup-plugin-wontache'
import hoganPlugin from './rollup-plugins/hogan.coffee'
import handlebarsPlugin from './rollup-plugins/handlebars.coffee'

engines = [
    'lodash'
    'underscore'
    'wontache'
    'hogan'
    'mustache'
    'handlebars'
]

commonPlugins = [
    coffee()
    nodeResolve()
    commonjs()
    babel
        babelHelpers: 'bundled'
        extensions: ['.coffee', '.js', '.mjs', '.cjs']
        presets: [['@babel/preset-env',
            forceAllTransforms: true
        ]]
]

commonOutput = [{
    plugins: [terser()]
    entryFileNames: '[name].mjs'
    dir: 'build'
}, {
    entryFileNames: '[name].cjs'
    dir: 'build'
    format: 'cjs'
}]

stage1config = [{
    input: "jit/#{engine}.coffee" for engine in engines
    plugins: [
        string include: '**/*.{mustache,hbs,jst}'
        commonPlugins...
    ]
    output: [{
        entryFileNames: 'jit.[name].js'
        chunkFileNames: 'jit.[name].js'
        dir: 'build'
    }]
}, {
    input: 'aot.lodash': 'aot/lodash.coffee'
    plugins: [
        lodashPlugin include: '**/*.jst', variable: '$'
        commonPlugins...
    ]
    output: commonOutput
}, {
    input: 'aot.underscore': 'aot/underscore.coffee'
    plugins: [
        underscorePlugin include: '**/*.jst', variable: '$'
        commonPlugins...
    ]
    output: commonOutput
}, {
    input: 'aot.wontache': 'aot/wontache.coffee'
    plugins: [
        wontachePlugin include: '**/*.mustache', precompile: true
        commonPlugins...
    ]
    output: commonOutput
}, {
    input: 'aot.hogan': 'aot/hogan.coffee'
    plugins: [
        hoganPlugin include: '**/*.mustache'
        commonPlugins...
    ]
    output: commonOutput
}, {
    input: 'aot.handlebars': 'aot/handlebars.coffee'
    plugins: [
        handlebarsPlugin include: '**/*.hbs'
        commonPlugins...
    ]
    output: commonOutput
}, {
    input: 'performance.coffee'
    plugins: [coffee(), nodeResolve(), commonjs()]
    output: [{
        file: 'performance.js'
    }]
}]

stage2config = for engine in engines
    input: "build/jit.#{engine}.js"
    treeshake: false
    output: commonOutput

do ->
    await each stage1config, bundleInput
    .catch console.error
    each stage2config, bundleInput
    .catch console.error
