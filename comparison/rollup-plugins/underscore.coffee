import mustache from 'wontache'
import { createFilter } from '@rollup/pluginutils'
import { template } from 'underscore'

moduleTemplate = mustache '''
    import _ from '../aot/customUnderscore/index-all.coffee';
    import JST from '../template/JST.coffee';
    export default {{&precompiled}};
'''

export default (options = {}) ->
    filter = createFilter options.include, options.exclude
    transform: (code, id) ->
        return unless filter id
        moduleTemplate precompiled: template(code, options).source
