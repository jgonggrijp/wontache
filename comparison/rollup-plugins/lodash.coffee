import mustache from 'wontache'
import { createFilter } from '@rollup/pluginutils'
template = import('lodash-es/template.js')

moduleTemplate = mustache '''
    import _ from '../aot/fakeLodash.coffee';
    import JST from '../template/JST.coffee';
    export default {{&precompiled}};
'''

export default (options = {}) ->
    filter = createFilter options.include, options.exclude
    transform: (code, id) ->
        return unless filter id
        compile = (await template).default
        moduleTemplate precompiled: compile(code, options).source
