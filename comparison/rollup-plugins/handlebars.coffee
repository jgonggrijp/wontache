import mustache from 'wontache'
import { createFilter } from '@rollup/pluginutils'
import Handlebars from 'handlebars/dist/cjs/handlebars.js'

moduleTemplate = mustache '''
    import Handlebars from 'handlebars/dist/cjs/handlebars.runtime.js';
    export default Handlebars.template({{&precompiled}});
'''

export default (options = {}) ->
    filter = createFilter options.include, options.exclude
    transform: (code, id) ->
        return unless filter id
        moduleTemplate precompiled: Handlebars.precompile code
