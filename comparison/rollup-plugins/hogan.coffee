import mustache from 'wontache'
import { createFilter } from '@rollup/pluginutils'
import Hogan from 'hogan.js'

moduleTemplate = mustache '''
    import Hogan from 'hogan.js';
    export default new Hogan.template({{&precompiled}});
'''

export default (options = {}) ->
    filter = createFilter options.include, options.exclude
    transform: (code, id) ->
        return unless filter id
        moduleTemplate precompiled: Hogan.compile code, asString: true
