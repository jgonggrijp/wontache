import JST from '../template/JST.coffee'

export run = (data) -> JST.invoice data

export runEach = (data) ->
    {invoice} = JST
    invoice d for d in data
