import Hogan from 'hogan.js'

import invoiceTemplate from '../template/invoice.mustache'
import partials from '../template/mustache-partials.coffee'

export run = (data) -> invoiceTemplate.render data, partials

export runEach = (data) -> invoiceTemplate.render d, partials for d in data
