import Handlebars from 'handlebars/dist/cjs/handlebars.runtime.js'

import invoiceTemplate from '../template/invoice.hbs'
import addressBaseTemplate from '../template/address-base.hbs'
import addressExtraTemplate from '../template/address-extra.hbs'
import addressInlineTemplate from '../template/address-inline.hbs'
import dateTemplate from '../template/date.hbs'

partials =
    'address-base': addressBaseTemplate
    'address-extra': addressExtraTemplate
    'address-inline': addressInlineTemplate
    date: dateTemplate

for name, partial of partials
    Handlebars.registerPartial name, partial

export run = (data) -> invoiceTemplate data

export runEach = (data) -> invoiceTemplate d for d in data
