import mustache from 'wontache'

import invoiceTemplate from '../template/invoice.mustache'
import partials from '../template/mustache-partials.coffee'

mustache.partials = partials

export run = (data) -> invoiceTemplate data

export runEach = (data) -> invoiceTemplate d for d in data
