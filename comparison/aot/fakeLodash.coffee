import escape from 'lodash-es/escape.js'
import each from 'lodash-es/each.js'

export default {escape, each}
