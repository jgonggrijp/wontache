import {
    extend, random, sample, toArray, chunk, values,
    map, reduce, times, chain, now
} from 'underscore'
import { Events } from 'backbone'
import dayjs from 'dayjs';

# All communication with external code will go through the `eventBus`. This
# enables asynchronous cooperation in a way that is portable between client and
# server and across ES versions. The `eventBus` is the default and only export.
eventBus = extend {}, Events

jitBundles =
    'jit:lodash': './build/jit.lodash.mjs'
    'jit:underscore': './build/jit.underscore.mjs'
    'jit:wontache': './build/jit.wontache.mjs'
    'jit:hogan': './build/jit.hogan.mjs'
    'jit:mustache': './build/jit.mustache.mjs'
    'jit:handlebars': './build/jit.handlebars.mjs'

aotBundles =
    'aot:lodash': './build/aot.lodash.mjs'
    'aot:underscore': './build/aot.underscore.mjs'
    'aot:wontache': './build/aot.wontache.mjs'
    'aot:hogan': './build/aot.hogan.mjs'
    'aot:handlebars': './build/aot.handlebars.mjs'

planetDefaults =
    tld: ['com']
    bic: ['BIMO']
    email: ['mail', 'mailer', 'allYouCanSend', 'paper-airplane']
    phone: '+c'

planetData = for p in [{
    name: 'B-612'
    tld: ['b612']
}, {
    name: 'Asteroid 325'
}, {
    name: 'Asteroid 326'
    tld: ['magnificent']
}, {
    name: 'Asteroid 327'
}, {
    name: 'Asteroid 328'
    tld: ['astra328']
}, {
    name: 'Asteroid 329'
}, {
    name: 'Asteroid 330'
}, {
    name: 'Mercury'
    cities: ['Sunside Mining Station']
    phone: '+a1'
}, {
    name: 'Venus'
    cities: ['HAVOC 2']
    phone: '+a2'
}, {
    name: 'Earth'
    tld: ['com', 'net', 'io', 'cc']
    bic: ['HSBC', 'DEUT', 'NEDS', 'BOFA', 'BNOR']
    phone: '+'
    cities: [
        'Ankara'
        'Marseille'
        'Oraibi'
        'Tauranga'
        'Sydney'
        'Beijing'
        'Yakutsk'
        'Moscow'
        'Hyderabad'
        'Hanoi'
        'Wadi Halfa'
        'Benin City'
        'Stone Town'
        'Cusco'
        'Flores'
        'Nuuk'
        'Longyearbyen'
        'Esperanza'
    ]
}, {
    name: 'Moon'
    tld: ['com', 'moon']
    bic: ['MOON']
    phone: '+b1'
    cities: ['Tranquility', 'Tycho']
}, {
    name: 'Mars'
    tld: ['mars', 'co.mars', 'net.mars']
    bic: ['MARS']
    phone: '+a4'
    cities: [
        'Olympus'
        'Alba'
        'Elysium'
        'Ascraeus'
        'Argyre'
        'Hellas'
        'Tithonium'
    ]
}, {
    name: 'Ceres'
    tld: ['ceres']
    cities: ['Ahuna']
}, {
    name: 'Io'
    tld: ['io']
    phone: '+b2'
    cities: ['Pele']
}, {
    name: 'Europa'
    phone: '+b3'
    cities: ['Sidon', 'Tyre']
}, {
    name: 'Ganymede'
    bic: ['GABA']
    phone: '+b4'
    cities: ['Gula', 'Anat', 'Uruk', 'Lagash']
}, {
    name: 'Callisto'
    phone: '+b5'
    cities: ['Burr', 'Lofn', 'Tindr']
}, {
    name: 'Titan'
    phone: '+b6'
    cities: ['Hano', 'Ontario', 'Selk']
}]
    extend {}, planetDefaults, p

streets = [
    'Curving Street'
    'Infinite Loop'
    'Diagon Alley'
    'Main Strait'
    'Via Appia'
    'Broadway'
]

firstNames = [
    'Antoine'
    'Antoinette'
    'Leon'
    'Leonie'
    'Little'
    'Thorny'
    'Fluffy'
    'Majestic'
    'John'
    'Jane'
]

lastNames = [
    'de Saint-Exupéry'
    'Werth'
    'Prince'
    'Princess'
    'Rose'
    'Sheep'
    'King'
    'Queen'
]

fullNames = [
    'The Astronomer'
    'Rose'
    'The Conceited'
    'The Tippler'
    'The Businessman'
    'The Businesswoman'
    'The Lamplighter'
    'The Geographer'
]

titles = ['mister', 'madam', 'doctor', 'majesty']

webOrderShops = [{
    name: 'SolarMart'
    domain: 'solar-mart'
}, {
    name: 'SpaceBay'
    domain: 'spacebay'
}, {
    name: 'The Space Guild'
    domain: 'sg'
}, {
    name: 'Megadodo'
    domain: 'megadodo'
}]

companyEpitheta = [
    ''
    ', Inc.'
    ' GmbH'
    ' S.A.'
    ' Ltd'
]

companyEmail = ['info', 'sales', 'hello', 'mail']
separatorTOS = '/#'
pageTOS = ['terms-and-conditions', 'terms-of-service', 'terms']

brandNames = [
    ''
    'Nature Books '
    'Mad Hatter '
    'Gardener '
    'FloralBest '
    'Little Lamb '
    'EyeSpy '
    'RoyalSatin '
    'Absolutely Fabulous '
    'KnockOut Booze '
    'Ixian '
    'Tleilaxu '
]

articles = [
    'True Stories from Nature'
    'fedora brown'
    'fedora blue'
    'fedora polka dots'
    'sombrero'
    'Boa constrictor, male'
    'Boa constrictor, female'
    'African elephant, male'
    'African elephant, female'
    'Indian elephant, male'
    'Indian elephant, female'
    'cardboard box 60 L'
    'wooden box 60 L'
    'plastic crate 60 L'
    'refractive telescope 100 mm'
    'refractive telescope 200 mm'
    'reflective telescope 100 mm'
    'reflective telescope 200 mm'
    'Turkish costume'
    'European costume'
    'shovel'
    'pruning shears'
    'weeder'
    'organic rose fertiliser 100mL'
    'sprinkling can 2 L'
    'draft screen 40x60 cm'
    'glass globe 60 cm'
    'trimmer blades'
    'sheep muzzle'
    'ermine robe L'
    'purple robe L'
    'golden sceptre'
    'golden crown'
    'golden throne'
    'queer hat'
    'beer, crate 24x250 mL'
    'Australian red wine 750 mL'
    'rum 1 L'
    'digital calculator'
    'fencing doll'
    'safety matches'
    'electric lighter'
    'LED deco lightbulb 800 lumen'
    'typewriter QUERTY'
    'typewriter Dvorak'
    'typewriter paper 80 g A4 500 sheets'
    'pencil HB 10 pc'
    'snake antivenom'
    'no more thirst 100 pc'
]

currencies = ['EUR', 'USD', 'JPY', 'GBP', 'CNY', 'stars']

randomNumber = (nDigits) -> random 10 ** nDigits - 1

randomDate = ->
    year = random 2019, 2040
    month = random 11
    day = random 1, 28
    date = dayjs new Date year, month, day
    return
        formatted: date.format 'MMMM D, YYYY'
        iso: date.format 'YYYY-MM-DD'

modIBAN = 91
alphabet = toArray 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
A = 'A'.charCodeAt() - 10
encodeIBAN = (str) -> c.charCodeAt() - A for c in str
randomIBAN = (bic) ->
    code = sample alphabet, 2
    account = randomNumber 7
    accountChunks = chain '' + account
    .chunk 4
    .map (chunk) -> chunk.join ''
    .join ' '
    encoded1 = +(encodeIBAN(bic).concat(account).join(''))
    check1 = encoded1 % modIBAN
    encoded2 = +([check1].concat(encodeIBAN(code), '00').join(''))
    check2 = encoded2 % modIBAN
    check = "#{if check2 < 10 then 0 else ''}#{check2}"
    "#{code.join ''} #{check} #{bic} #{accountChunks}"

randomAddress = (locality) ->
    planet = locality.name
    {cities} = locality
    return {planet} unless cities?
    city = sample cities
    address = "#{sample streets} #{randomNumber 3}"
    zip_code = randomNumber 5
    {planet, city, address, zip_code}

spaces = /\ /g
randomConsumer = (locality, address) ->
    client_id = randomNumber 7
    persona = if random 1 then name: sample fullNames else
        first_name: sample firstNames
        last_name: sample lastNames
    title = sample titles
    mailAccount = values(persona).join('.').replace(spaces, '').toLowerCase()
    mailProvider = sample locality.email
    tld = sample locality.tld
    email = "#{mailAccount}@#{mailProvider}.#{tld}"
    extend persona, {client_id, title, email}, address

randomCompany = (locality, address, companyBase) ->
    name = "#{companyBase.name}#{sample companyEpitheta}"
    phone = "#{locality.phone}#{randomNumber 12}"
    domain = "#{companyBase.domain}.#{sample locality.tld}"
    email = "#{sample companyEmail}@#{domain}"
    web = "https://#{domain}"
    extend {name, phone, email, web}, address

small = -> random 1, 2
large = -> random 10, 20

randomShipping = (sampleSize) ->
    for name in sample articles, sampleSize()
        article_id = randomNumber 6
        description = "#{sample brandNames}#{name}"
        warranty = randomDate()
        price = randomNumber(5)
        vat = random 25
        amount = sampleSize()
        subtotal = price * amount
        price = (price / 100).toFixed 2
        {article_id, description, warranty, price, vat, amount, subtotal}

addUp = (runningTotal, shipItem) ->
    subtotal = runningTotal.subtotal + shipItem.subtotal
    vat = runningTotal.vat + shipItem.subtotal * shipItem.vat
    shipItem.subtotal = (shipItem.subtotal / 100).toFixed 2
    {subtotal, vat}

randomDelivery = (recipientAddress, sampleSize) ->
    delivery_id = randomNumber 15
    date = randomDate()
    shipped_to = if 9 > random 9
        recipientAddress
    else
        randomAddress sample planetData
    delivered = (3 > random 3)
    shipping_list = randomShipping sampleSize
    delivery = {delivery_id, date, shipped_to, delivered, shipping_list}
    {subtotal, vat} = reduce shipping_list, addUp, subtotal: 0, vat: 0
    vat = vat / subtotal
    {delivery, subtotal, vat}

randomInvoice = (sampleSize) ->
    type = 'invoice'
    order_id = randomNumber 9
    date = randomDate()
    currency = sample currencies
    creditorBase = sample webOrderShops
    company = creditorBase.name
    creditorLocality = sample planetData
    IBAN = randomIBAN sample creditorLocality.bic
    creditorAddress = randomAddress creditorLocality
    contact = randomCompany creditorLocality, creditorAddress, creditorBase
    TOS = "#{contact.web}#{sample separatorTOS}#{sample pageTOS}"
    debitorLocality = sample planetData
    debitorAddress = randomAddress debitorLocality
    recipient = if sampleSize is small
        randomConsumer debitorLocality, debitorAddress
    else
        randomCompany debitorLocality, debitorAddress, sample webOrderShops
    deliveries = times sampleSize(), ->
        randomDelivery debitorAddress, sampleSize
    {subtotal, vat} = reduce deliveries, addUp, subtotal: 0, vat: 0
    total_vat_percentage = (vat / subtotal).toFixed 1
    total_vat_amount = (vat / 10000).toFixed 2
    total_amount = (subtotal / 100).toFixed 2
    pay_before = randomDate()
    deliveries = map deliveries, 'delivery'
    {
        type, order_id, date, currency, total_amount,
        total_vat_percentage, total_vat_amount, pay_before,
        company, IBAN, TOS, contact, recipient, deliveries
    }

generateSampleData = () ->
    eventBus.trigger 'sampleData:start'
    sampleData =
        small: randomInvoice small for [0...4000]
        large: randomInvoice large for [0...200]
    eventBus.trigger 'sampleData:finish', sampleData
    sampleData

warmup = (bundle, dataset) ->
    present = past = current = previous = now()
    loop
        bundle.runEach dataset for [0...5]
        [present, past] = [now(), present]
        [current, previous] = [present - past, current]
        if current >= previous then return

gcBreak = (delay) ->
    # give garbage collection an opportunity to kick in so it doesn't interfere
    new Promise (resolve) -> setTimeout resolve, delay

bench = (bundle, dataset) ->
    await gcBreak 20
    current = start = now()
    end = start + 1000
    runs = 0
    while runs < 10 or current < end
        bundle.runEach dataset
        ++runs
        current = now()
    (current - start) / (runs * dataset.length)

testBundle = (bundle, name, sampleData) ->
    eventBus.trigger 'warmup:small', name
    warmup bundle, sampleData.small
    eventBus.trigger 'runSmall:start', name
    resultSmall = await bench bundle, sampleData.small
    eventBus.trigger 'runSmall:finish', name, resultSmall
    eventBus.trigger 'warmup:large', name
    warmup bundle, sampleData.large
    eventBus.trigger 'runLarge:start', name
    resultLarge = await bench bundle, sampleData.large
    eventBus.trigger 'runLarge:finish', name, resultLarge

testJitBundle = (bundle, name, sampleData) ->
    eventBus.trigger 'compile:start', name
    beforeCompile = now()
    bundle.compile()
    eventBus.trigger 'compile:finish', name, now() - beforeCompile
    testBundle bundle, name, sampleData

runBenchmark = ->
    sampleData = generateSampleData()
    for name, path of jitBundles
        eventBus.trigger 'import:start', name
        bundle = await import(path)
        await gcBreak 50
        await testJitBundle bundle, name, sampleData
    for name, path of aotBundles
        eventBus.trigger 'import:start', name
        bundle = await import(path)
        await gcBreak 50
        await testBundle bundle, name, sampleData

eventBus.on 'runSmall:finish', (name, result) ->
    console.info "#{name}, small: #{result} ms/invoice"

eventBus.on 'runLarge:finish', (name, result) ->
    console.info "#{name}, large: #{result} ms/invoice"

runBenchmark()
