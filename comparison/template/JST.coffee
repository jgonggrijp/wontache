import addressBaseTemplate from './address-base.jst'
import addressExtraTemplate from './address-extra.jst'
import addressInlineTemplate from './address-inline.jst'
import dateTemplate from './date.jst'
import invoiceTemplate from './invoice.jst'

export default
    addressBase: addressBaseTemplate
    addressExtra: addressExtraTemplate
    addressInline: addressInlineTemplate
    date: dateTemplate
    invoice: invoiceTemplate
