import addressBaseTemplate from './address-base.mustache'
import addressExtraTemplate from './address-extra.mustache'
import addressInlineTemplate from './address-inline.mustache'
import dateTemplate from './date.mustache'

export default
    'address-base': addressBaseTemplate
    'address-extra': addressExtraTemplate
    'address-inline': addressInlineTemplate
    date: dateTemplate
