import Hogan from 'hogan.js'

import invoiceTemplate from '../template/invoice.mustache'
import partials from '../template/mustache-partials.coffee'

template = null

export compile = ->
    for name, template of partials
        partials[name] = Hogan.compile template
    template = Hogan.compile invoiceTemplate

export run = (data) -> template.render data, partials

export runEach = (data) -> template.render d, partials for d in data
