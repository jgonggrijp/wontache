import mustache from 'wontache'

import invoiceTemplate from '../template/invoice.mustache'
import partials from '../template/mustache-partials.coffee'

template = null

export compile = ->
    for name, partial of partials
        partials[name] = mustache partial
    mustache.partials = partials
    template = mustache invoiceTemplate

export run = (data) -> template data

export runEach = (data) -> template d for d in data
