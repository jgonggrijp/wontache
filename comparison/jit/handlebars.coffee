import Handlebars from 'handlebars/dist/cjs/handlebars.js'

import invoiceTemplate from '../template/invoice.hbs'
import addressBaseTemplate from '../template/address-base.hbs'
import addressExtraTemplate from '../template/address-extra.hbs'
import addressInlineTemplate from '../template/address-inline.hbs'
import dateTemplate from '../template/date.hbs'

partials =
    'address-base': addressBaseTemplate
    'address-extra': addressExtraTemplate
    'address-inline': addressInlineTemplate
    date: dateTemplate

template = null

export compile = ->
    for name, partial of partials
        Handlebars.registerPartial name, Handlebars.compile partial
    template = Handlebars.compile invoiceTemplate

export run = (data) -> template data

export runEach = (data) -> template d for d in data
