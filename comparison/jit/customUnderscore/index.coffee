export { default as each,
         default as forEach } from 'underscore/modules/each.js'
export { default as escape } from 'underscore/modules/escape.js'
export { default as template } from 'underscore/modules/template.js'
