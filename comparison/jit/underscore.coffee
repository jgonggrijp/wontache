import { template } from './customUnderscore/index-all.coffee'

import JST from '../template/JST.coffee'

export compile = () ->
    for name, tpl of JST
        JST[name] = template tpl, variable: '$'
    (globalThis ? global ? self).JST = JST

export run = (data) -> JST.invoice data

export runEach = (data) ->
    {invoice} = JST
    invoice d for d in data
