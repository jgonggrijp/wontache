import Mustache from 'mustache'

import invoiceTemplate from '../template/invoice.mustache'
import partials from '../template/mustache-partials.coffee'

export compile = ->
    Mustache.parse template for name, template of partials
    Mustache.parse invoiceTemplate

export run = (data) -> Mustache.render invoiceTemplate, data, partials

export runEach = (data) ->
    Mustache.render invoiceTemplate, d, partials for d in data
