import template from 'lodash-es/template.js'
# Unlike Underscore, Lodash doesn't let you have a custom build with all of the
# following properties:
#
# 1. tree-shrunken, i.e., significantly smaller than Lodash's 26 KB minified and
#    gzipped of the default build;
# 2. fully functional _ wrapper with chaining;
# 3. the _.template function.
#
# Since point 3 is non-optional and we are trying to demonstrate the absolute
# minimum size required (point 1), we give up on the wrapper and use Lodash's
# `imports` parameter instead in order to provide `_` as a limited namespace
# handle inside the template. Below, we import the ingredients for this minimal
# namespace handle.
import escape from 'lodash-es/escape.js'
import each from 'lodash-es/each.js'

import JST from '../template/JST.coffee'

export compile = () ->
    for name, tpl of JST
        JST[name] = template tpl,
            variable: '$'
            imports: {JST, _: {escape, each}}

export run = (data) -> JST.invoice data

export runEach = (data) ->
    {invoice} = JST
    invoice d for d in data
