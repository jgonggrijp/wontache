# mustache-engine-comparison

*Comparison between six popular JavaScript template engines*

This is a private [workspace][workspaces] within the [Wontache][wontache] project. It has two goals:

- For end users, to provide a web page where they can learn about the strengths and weaknesses of the Wontache engine, as compared to other template engines for JavaScript.
- For developers, to help assess the impact of code changes on the performance of the Wontache engine.

This is still a work in progress.

[wontache]: https://gitlab.com/jgonggrijp/wontache
[workspaces]: https://classic.yarnpkg.com/en/docs/workspaces
