# Wontache website

The contents of this directory are updated through our [continuous deployment][ci-yml] and then [published][website] using [GitLab Pages][pages].

[ci-yml]: https://gitlab.com/jgonggrijp/wontache/-/blob/integration/.gitlab-ci.yml
[website]: https://jgonggrijp.gitlab.io/wontache/
[pages]: https://gitlab.com/help/user/project/pages/index

The following files are curated manually:

- `index.css`
- `prism.js`

The following files are generated from other sources using our own tools:

- `index.html` (from `../mustache/README.md`)

The following files are derived through other means:

- `mustache.5.html` (copied from the `../ruby-mustache` submodule, where we keep it up-to-date)
