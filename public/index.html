<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Wontache</title>
    <link rel=stylesheet href="index.css">
</head><body>
    <p><a href="https://www.npmjs.com/package/wontache"><img src="https://img.shields.io/npm/v/wontache" alt="latest version on npm"></a> <a href="https://juliangonggrijp.com"><img src="https://img.shields.io/badge/author-Julian_Gonggrijp-708" alt="author: Julian Gonggrijp"></a> <a href="https://gitlab.com/jgonggrijp/wontache/blob/master/LICENSE"><img src="https://img.shields.io/npm/l/wontache" alt="license text"></a> <a href="https://gitlab.com/jgonggrijp/wontache"><img src="https://img.shields.io/badge/-GitLab-555?logo=gitlab" alt="code hosted on GitLab"></a> <a href="https://gitlab.com/jgonggrijp/wontache/-/releases"><img src="https://img.shields.io/badge/-change_log-555?logo=gitlab" alt="changelog"></a> <a href="https://gitlab.com/jgonggrijp/wontache/issues"><img src="https://img.shields.io/badge/-issues-555?logo=gitlab" alt="issue tracker on GitLab"></a> <a href="https://gitlab.com/jgonggrijp/wontache/-/commits/integration"><img src="https://img.shields.io/gitlab/pipeline-status/jgonggrijp/wontache?branch=integration&label=pipeline" alt="pipeline status"></a></p>
    <h1 id="wontache">Wontache</h1>
    <p><em>Compact, spec-compliant Mustache implementation with extras</em></p>
    <p>Wontache is a fast, precompiling implementation of the <a href="http://mustache.github.io">Mustache</a> templating language for JavaScript, written in just <a href="https://gitlab.com/jgonggrijp/wontache/-/blob/integration/mustache/mustache.coffee">a few hundred lines of literate CoffeeScript</a>. It fully implements version 1.3.0 of the <a href="https://github.com/mustache/spec">Mustache specification</a>, including the optional modules, as well as some extra features which are listed further down below. An up-to-date reference of the Mustache templating language is available <a href="mustache.5.html">over here</a>. You can try out the Wontache engine in its full glory, right from your browser, in the <a href="playground.html">playground</a>.</p>
    <p>Compared to:</p>
    <ul>
    <li><a href="https://www.npmjs.com/package/handlebars">Handlebars.js</a>, Wontache is much smaller, substantially faster and faithful to Mustache’s original syntax and semantics.</li>
    <li><a href="https://www.npmjs.com/package/mustache">Mustache.js</a>, Wontache is much more accurate at indentation handling, substantially faster and committed to adhere to the official specification.</li>
    <li><a href="https://www.npmjs.com/package/hogan.js">Hogan.js</a>, Wontache is more up-to-date with the specification, more accurate at indentation handling and a bit faster on average, but overall similar in spirit. While I was unaware of Hogan.js when I started writing Wontache and the libraries have no common heritage, I hope that Wontache can be a worthy successor of Hogan.js, which is end-of-life.</li>
    </ul>
    <p>The above claims are based on extensive benchmarks and rendering comparisons. There is no user-friendly presentation of the comparison yet, but if you wish, you can inspect the <a href="https://gitlab.com/jgonggrijp/wontache/-/tree/integration/comparison">comparison source code</a>. Of course, the best way to determine how Wontache performs in your application is to try it.</p>
    <ul>
    <li><a href="#quickstart">Quickstart</a></li>
    <li><a href="#development-status">Development status</a></li>
    <li><a href="#extras">Extras</a></li>
    <li><a href="#companion-libraries-and-tools">Companion libraries and tools</a></li>
    <li><a href="#build-variants">Build variants</a></li>
    <li><a href="#underscore-dependency">Underscore dependency</a></li>
    <li><a href="#manual">Manual</a><ul>
    <li><a href="#compilation">Compilation</a></li>
    <li><a href="#rendering">Rendering</a></li>
    <li><a href="#partials">Partials</a></li>
    <li><a href="#changing-the-delimiters">Changing the delimiters</a></li>
    <li><a href="#precompilation">Precompilation</a></li>
    <li><a href="#wrapmodule"><code>wrapModule</code></a></li>
    <li><a href="#decompilation">Decompilation</a></li>
    </ul>
    </li>
    <li><a href="#credits">Credits</a></li>
    </ul>
    <h2 id="quickstart">Quickstart</h2>
    <pre><code class="language-js">
    import mustache from &#39;wontache&#39;;

    const template = &#39;{{#people}}Hello {{name}}!\n{{/people}}&#39;;
    const data = {
        people: [
            {name: &#39;Alice&#39;},
            {name: &#39;Bob&#39;},
        ],
    };

    const templateFunction = mustache(template);
    const output = templateFunction(data);
    // Hello Alice!
    // Hello Bob!

    // You can also do a double call for a quick one-off:
    output = mustache(template)(data);
    </code></pre>
    <h2 id="development-status">Development status</h2>
    <p>The 0.x release series is intended for evaluation. Wontache already completely implements the latest Mustache <a href="https://github.com/mustache/spec">specification</a> and is fully <a href="https://gitlab.com/jgonggrijp/wontache/-/commits/integration">tested</a>. Templates that compile in Wontache 0.x are expected to also compile in the 1.x release series and  should mostly produce the same output. However, the interface has not fully stabilized yet. Among other things, this means that you might need to recompile your templates when Wontache is updated.</p>
    <p>Most of the current development work is focused on adding production-friendly conveniences, such as TypeScript and Flow type declarations, a command line tool and integrations for various code bundling tools. More extensive documentation is planned as well. You can track progress towards the official launch <a href="https://gitlab.com/jgonggrijp/wontache/-/milestones/2">over here</a>.</p>
    <h2 id="extras">Extras</h2>
    <ul>
    <li>Intelligently handles <a href="mustache.5.html#Blocks">block</a> indentation in the context of <a href="https://github.com/mustache/spec/blob/v1.2.2/specs/%7Einheritance.yml">inheritance</a>, in accordance with a <a href="https://github.com/mustache/spec/pull/131">proposed extension</a> by the author.</li>
    <li><em>new in version 0.1.0 (integrated in version 1.3.0 of the spec)</em> Implements <a href="https://github.com/mustache/spec/pull/134">dynamic names</a> in <a href="mustache.5.html#Partials">partials</a> and <a href="mustache.5.html#Parents">parents</a>, allowing you to write <code>{{&gt;*name}}</code> to mean that <code>name</code> should be looked up in the context to find the actual name of the partial that should be interpolated. <code>name</code> may also be a dotted name or the implicit iterator <code>.</code>.</li>
    </ul>
    <p>You can track planned extras <a href="https://gitlab.com/jgonggrijp/wontache/-/issues?label_name%5B%5D=enhancement">over here</a>.</p>
    <h2 id="companion-libraries-and-tools">Companion libraries and tools</h2>
    <ul>
    <li><a href="plugin-rollup.html"><code>rollup-plugin-wontache</code></a> lets you bundle Mustache template files with your JavaScript code using Rollup.</li>
    <li><a href="plugin-webpack.html"><code>wontache-loader</code></a> lets you bundle Mustache template files with your JavaScript code using webpack.</li>
    </ul>
    <p>Integrations for more bundling and task automation tools are planned, as well as other companion libraries and tools.</p>
    <h2 id="build-variants">Build variants</h2>
    <p>Wontache has several build variants. If you use the package in Node.js or bundle it with a Node.js-aware tool such as Rollup, WebPack or Browserify, the right build variant will be selected automatically. In other situations, such as loading from a CDN, using an AMD loader or usage in deno, you may need to select a build variant explicitly. The following build variants are of interest outside of Node.js:</p>
    <ul>
    <li><code>mustache-umd.js</code> is an old-fashioned UMD module. When not using CommonJS or AMD, the <code>mustache</code> function is globally available as <code>_.mustache</code>. The global name is still subject to change.</li>
    <li><code>mustache-esm.js</code> is an ES module that imports the Underscore library as a monolithic interface. This variant is appropriate for direct use in ESM environments.</li>
    <li><code>module.js</code> is an ES module that imports individual Underscore functions from their respective modules. This variant is suitable for inclusion in a custom Underscore. More on Underscore below.</li>
    </ul>
    <h2 id="underscore-dependency">Underscore dependency</h2>
    <p>Wontache depends on <a href="https://underscorejs.org">Underscore</a> for some utility functions. This enables us to write shorter, more maintainable, more portable and higher quality code than if the library was dependency-free. We encourage everyone to embrace Underscore (and other libraries) for these same reasons.</p>
    <p>If you are concerned about dependency size, you can use the <code>module.js</code> build variant in order to enable treeshaking. This is the <code>module</code> entry in the <code>package.json</code>, so bundlers like Rollup and WebPack will find it automatically. The subset of Underscore that Wontache depends on is only about 190 lines of ES3, including blank lines and comments.</p>
    <p>Alternatively, the package includes a <code>customUnderscore</code> subdirectory with an <code>index.js</code> that lists all of our direct and indirect Underscore dependencies. You can use this in order to compose your own <a href="https://juliangonggrijp.com/article/introducing-modular-underscore.html">custom Underscore</a>.</p>
    <p>Wontache should also work with Lodash, although this is untested.</p>
    <h2 id="manual">Manual</h2>
    <h3 id="compilation">Compilation</h3>
    <p>The <code>default</code> and only export is a function that we call <code>mustache</code> by convention. It takes a Mustache template string as first argument and returns the compiled template as a function:</p>
    <pre><code class="language-js">
    import mustache from &#39;wontache&#39;;

    var template = &#39;template with {{variable}}&#39;;
    var compiled = mustache(template);
    </code></pre>
    <p>The <a href="plugin-rollup.html">Rollup plugin</a> and the <a href="plugin-webpack.html">webpack loader</a> will generate the above code automatically and wrap it in a module, so you can import the compiled template directly from a standalone template file:</p>
    <pre><code class="language-mustache">
    {{! template.mustache }}
    template with {{variable}}
    </code></pre>
    <pre><code class="language-js">
    import compiled from &#39;./template.mustache&#39;;
    </code></pre>
    <h3 id="rendering">Rendering</h3>
    <p>The compiled template function takes the input data as its first argument. This can be any JavaScript value. Values to interpolate in the template are taken from the data. The template function returns a string with the final result of the template, given the data.</p>
    <pre><code class="language-js">
    compiled({variable: &#39;flair&#39;});
    // &#39;template with flair&#39;
    </code></pre>
    <h3 id="partials">Partials</h3>
    <p>The <a href="mustache.5.html#Partials">Partial</a> and <a href="mustache.5.html#Parents">Parent</a> tags let you render and interpolate a template inside another template. If your templates contain either of those tags, you have to do some administration so that compiled templates are able to find each other by name. This administration takes the form of an object, where each key is the name of a template that may be embedded. The corresponding value may be either a template string or a template function (if you set a template string, it will be compiled on first use).</p>
    <pre><code class="language-js">
    // The administration.
    var namedTemplates = {
        link: &#39;&lt;a href=&quot;{{&amp;url}}&quot;&gt;{{title}}&lt;/a&gt;&#39;
    };

    // A template that will need the above administration.
    var template = &#39;&lt;ul&gt;{{#.}}&lt;li&gt;{{&gt;link}}{{/.}}&lt;/ul&gt;&#39;;
    var linkList = mustache(template);

    // The data.
    var links = [{
        url: &#39;https://jgonggrijp.gitlab.io/wontache/&#39;,
        title: &#39;Wontache home page&#39;
    }];
    </code></pre>
    <p>There are two possible ways to make the partial administration available to a template function. The most <em>hygienic</em> way is to pass an object with a <code>partials</code> property as the second argument in the call to the template. This approach is <strong>strongly recommended</strong> for library authors, because it ensures that the partials provided to your template are completely isolated from the partials that other libraries or the application may be using.</p>
    <pre><code class="language-js">
    linkList(links, {partials: namedTemplates});
    // &#39;&lt;ul&gt;&lt;li&gt;&lt;a href=&quot;https://jgonggrijp.gitlab.io/wontache/&quot;&gt;Wontache home page&lt;/a&gt;&lt;/ul&gt;&#39;
    </code></pre>
    <p>If desired, you can write a wrapper function that always passes the same partial administration to a given template function.</p>
    <pre><code class="language-js">
    function render(templateFunc, data) {
        return templateFunc(data, {partials: namedTemplates});
    }

    render(linkList, links);
    // Same output as above (&#39;&lt;ul&gt;&lt;li&gt;&lt;a href=&quot;...&lt;/a&gt;&lt;/ul&gt;&#39;).
    </code></pre>
    <p>The most <em>convenient</em> way is to assign your administration to <code>mustache.partials</code>. Template functions automatically fall back to this if you don’t pass a set of partials explicitly, so it can be “set and forget”. This approach is intended for application authors. When using it, do keep in mind that <code>mustache.partials</code> is easy to compromise, by overwriting either one of its keys or the property as a whole. In principle, a sloppy library author could do this as well. You may even want to dedicate a test to ensuring that <code>mustache.partials</code> is complete.</p>
    <pre><code class="language-js">
    Object.assign(mustache.partials, namedTemplates);
    // Could also use Underscore&#39;s _.extend for compatibility.

    linkList(links);
    // Same output again (&#39;&lt;ul&gt;&lt;li&gt;&lt;a href=&quot;...&lt;/a&gt;&lt;/ul&gt;&#39;).
    </code></pre>
    <h3 id="changing-the-delimiters">Changing the delimiters</h3>
    <p>The <code>{{</code> and <code>}}</code> default delimiters were chosen for a low probability of conflict with other computer languages. However, a conflict is still possible, for example when the template is meant to generate LaTeX code, or when you are writing a template that includes example Mustache template code that should be rendered verbatim. In such cases, you can change the delimiters.</p>
    <p>The normal way to change the delimiters is by including a <a href="mustache.5.html#Set-Delimiter">Set Delimiter tag</a> in the template text. This is recommended in most cases, because this ensures your templates are portable to other Mustache implementations. It also gives you the freedom to switch delimiters halfway through a template, even multiple times if necessary.</p>
    <pre><code class="language-js">
    var template = &#39;{{=&lt; &gt;=}} template with &lt;variable&gt;&#39;;
    var compiled = mustache(template);
    </code></pre>
    <p>However, you may have a large application with many templates. If there is a specific, alternative set of delimiters that you are consistently using in each of them, you probably don’t want to start each template with the same Set Delimiter tag. For this purpose, you can pass the alternative delimiters as a second argument to <code>mustache</code> instead.</p>
    <pre><code class="language-js">
    var template = &#39;template with &lt;variable&gt;&#39;;
    var compiled = mustache(template, [&#39;&lt;&#39;, &#39;&gt;&#39;]);
    </code></pre>
    <p>If you are also calling <code>mustache</code> in many places, you can wrap the function so you don’t have to explicitly pass the delimiters every time:</p>
    <pre><code class="language-js">
    var myMustache = template =&gt; mustache(template, [&#39;&lt;&#39;, &#39;&gt;&#39;]);

    // Equivalent, using Underscore:

    import _, { partial } from &#39;underscore&#39;;

    var myMustache = partial(mustache, _, [&#39;&lt;&#39;, &#39;&gt;&#39;]);

    // usage in either case:

    var compiled = myMustache(&#39;template with &lt;variable&gt;&#39;);
    </code></pre>
    <h3 id="precompilation">Precompilation</h3>
    <p>Template <a href="#compilation">compilation</a> is a costly operation. Precompilation is an <strong>optimization</strong> that lets you do the compilation ahead of time.</p>
    <p>A common situation where you might want to use precompilation, is in a client side web application. The application will respond faster if it does not need to compile its templates before <a href="#rendering">rendering</a> them. Precompilation lets you do the compilation already before the application code is sent to the client, for example in a serverside process that bundles the code.</p>
    <p>Keep in mind that the precompiled template code is larger than the original template text. You save startup time at the receiving end, at the expense of transferring more data.</p>
    <p>The easiest way to use precompilation, is to set <code>precompile: true</code> when using the <a href="plugin-rollup.html">Rollup plugin</a> or the <a href="plugin-webpack.html">webpack loader</a>. However, you can also tap directly in the underlying mechanisms if you need a custom solution.</p>
    <p>As described <a href="#compilation">above</a>, when you compile a template the “regular” way, you obtain a function that will render the template. This function has a <code>source</code> property, which is a string that encodes a JavaScript object. That object contains the end result of compilation. It can be passed to <code>mustache</code> instead of the original template text, in order to recreate the compiled template function. In other words, if you build a string <code>precompiled</code> as follows,</p>
    <pre><code class="language-js">
    var precompiled = &quot;import mustache from &#39;wontache&#39;;\n&quot; +
    &quot;export default mustache(&quot; + compiled.source + &quot;);&quot;;
    </code></pre>
    <p>and you write <code>precompiled</code> to a file, you have created a new JavaScript module that exports <code>compiled</code>. No compilation needs to be done in that module; all that work was already done when you created <code>compiled</code> the first time, before writing the module code to disk.</p>
    <p><code>compiled.source</code> does not include the outer <code>mustache()</code> function call in order to give you freedom. For example, you could name the <code>mustache</code> import differently, or you could write JavaScript code that has the precompiled objects in an array rather than passing them directly to <code>mustache</code>.</p>
    <p>If writing JavaScript modules is exactly what you want to do, however, you do not need to build the strings yourself. Instead, you can use <a href="#wrapmodule"><code>wrapModule</code></a>, as described in the following section.</p>
    <h3 id="wrapmodule">wrapModule</h3>
    <p>The <a href="plugin-rollup.html">Rollup plugin</a> and integrations for other build tools all do roughly the same thing: take a Mustache template and wrap it as a JavaScript module. The common logic is contained in the <code>wrapModule</code> function, which is the default and only export of the auxiliary module <code>wontache/wrap-module</code>.</p>
    <p><code>wrapModule</code> takes the raw template string as the first argument and an object with options as an optional second argument. It returns a string with the JavaScript module code. The following options are available.</p>
    <ul>
    <li><code>precompile</code>: boolean to indicate whether you want to use <a href="#precompilation">precompilation</a>, default <code>false</code>.</li>
    <li><code>type</code>: string naming the module type, either <code>&#39;ESM&#39;</code> (default), <code>&#39;AMD&#39;</code> or <code>&#39;CommonJS&#39;</code>.</li>
    <li><code>delimiters</code>: the <a href="#changing-the-delimiters">delimiters</a> that the compiler should start parsing with, default <code>[&#39;{{&#39;, &#39;}}&#39;]</code>.</li>
    <li><code>wontache</code>: the name used for the default import from Wontache, default <code>&#39;mustache&#39;</code>.</li>
    </ul>
    <pre><code class="language-js">
    import wrapModule from &#39;wontache/wrap-module&#39;;

    const wrappedModule = wrapModule(template, {precompile: true});
    // import mustache from &#39;wontache&#39;;
    // export default mustache(...);
    </code></pre>
    <h3 id="decompilation">Decompilation</h3>
    <p>Since templates sometimes need to be recompiled, there is a “backdoor” of sorts to reconstruct the original template text from the <a href="#compilation">compiled</a> function. While this is mostly an implementation detail, it might occasionally be useful for debugging purposes.</p>
    <pre><code class="language-js">
    compiled(null, {decompile: true});
    // &#39;template with {{variable}}&#39;
    </code></pre>
    <h2 id="credits">Credits</h2>
    <p>The name “Wontache” was suggested by my dear friend Arie de Bruin.</p>
    <script src="prism.js"></script>
</body></html>