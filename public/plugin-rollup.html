<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>rollup-plugin-wontache</title>
    <link rel=stylesheet href="index.css">
</head><body>
    <p><a href="https://www.npmjs.com/package/rollup-plugin-wontache"><img src="https://img.shields.io/npm/v/rollup-plugin-wontache" alt="latest version on npm"></a> <a href="https://juliangonggrijp.com"><img src="https://img.shields.io/badge/author-Julian_Gonggrijp-708" alt="author: Julian Gonggrijp"></a> <a href="https://gitlab.com/jgonggrijp/wontache/blob/master/LICENSE"><img src="https://img.shields.io/npm/l/rollup-plugin-wontache" alt="license text"></a> <a href="https://gitlab.com/jgonggrijp/wontache"><img src="https://img.shields.io/badge/-GitLab-555?logo=gitlab" alt="code hosted on GitLab"></a> <a href="https://gitlab.com/jgonggrijp/wontache/-/releases"><img src="https://img.shields.io/badge/-change_log-555?logo=gitlab" alt="changelog"></a> <a href="https://gitlab.com/jgonggrijp/wontache/issues"><img src="https://img.shields.io/badge/-issues-555?logo=gitlab" alt="issue tracker on GitLab"></a> <a href="https://gitlab.com/jgonggrijp/wontache/-/commits/integration"><img src="https://img.shields.io/gitlab/pipeline-status/jgonggrijp/wontache?branch=integration&label=pipeline" alt="pipeline status"></a></p>
    <h1 id="rollup-plugin-wontache">rollup-plugin-wontache</h1>
    <p><em><a href="https://rollupjs.org">Rollup</a> plugin for bundling <a href="http://mustache.github.io">Mustache</a> templates with the <a href="index.html">Wontache</a> engine</em></p>
    <h2 id="quickstart">Quickstart</h2>
    <p>Your template (<code>hello.mustache</code>):</p>
    <pre><code class="language-mustache">
    Hello, {{name}}!
    </code></pre>
    <p>Your code, which uses the template (<code>index.js</code>):</p>
    <pre><code class="language-js">
    import hello from &#39;./hello.mustache&#39;;
    console.log(hello({name: &#39;World&#39;}));
    </code></pre>
    <p>Your <code>rollup.config.js</code>:</p>
    <pre><code class="language-js">
    import wontache from &#39;rollup-plugin-wontache&#39;;

    export default {
        input: &#39;./index.js&#39;,
        plugins: [wontache()],
        // ...
    };
    </code></pre>
    <h2 id="options">Options</h2>
    <pre><code class="language-js">
    import wontache from &#39;rollup-plugin-wontache&#39;;

    // ...
        wontache({
            include: &#39;**/*.mustache&#39;,
            exclude: [],
            precompile: false,
            delimiters: [&#39;{{&#39;, &#39;}}&#39;],
            wontache: &#39;mustache&#39;,
        })
    </code></pre>
    <h3 id="include">include</h3>
    <p><em>Glob string, or array of glob strings</em> &mdash; <strong>Default:</strong> <code>&#39;**/*.mustache&#39;</code></p>
    <p>Transform only modules of which the file name matches the given pattern(s). Patterns are matched against the absolute path on disk. An empty array will include everything.</p>
    <h3 id="exclude">exclude</h3>
    <p><em>Glob string, or array of glob strings</em> &mdash; <strong>Default:</strong> <code>[]</code></p>
    <p>Skip modules of which the file name matches the given pattern(s). Patterns are matched against the absolute path on disk. An empty array will skip nothing.</p>
    <h3 id="precompile">precompile</h3>
    <p><em>Boolean</em> &mdash; <strong>Default:</strong> <code>false</code></p>
    <p>This option only controls an optimization. The transformed module will always export a compiled, ready-to-run template function, regardless of whether you pass <code>true</code> or <code>false</code>.</p>
    <p>If <code>false</code>, the compilation happens during initial loading of the transformed module. If <code>true</code>, the compilation already happens while Rollup is performing the transform, so the transformed module does not need to perform this work anymore.</p>
    <p>The optimization is off by default, because it also has a cost: the precompiled version of a template is always larger than the original template text. Leave it off to keep your bundle as small as possible. Try switching it on when you find that there is too much delay during initial loading of the bundle. If you are publishing a library that includes bundled templates, consider giving your users a choice between bundles with and without precompilation.</p>
    <p>If you want the transformed module to export the template as a string, instead of as an already compiled function, use <a href="https://github.com/TrySound/rollup-plugin-string"><code>rollup-plugin-string</code></a> instead.</p>
    <h3 id="delimiters">delimiters</h3>
    <p><em>Array of exactly two nonempty strings</em> &mdash; <strong>Default:</strong> <code>[&#39;{{&#39;, &#39;}}&#39;]</code></p>
    <p>Override this to change the <a href="index.html#changing-the-delimiters">delimiters</a> with which your templates will be parsed initially, until encountering <a href="mustache.5.html#Set-Delimiter">Set Delimiter tags</a>.</p>
    <p>Your template with alternative delimiters:</p>
    <pre><code class="language-mustache">
    Hello, [name]!
    </code></pre>
    <p>Your override in <code>rollup.config.js</code>:</p>
    <pre><code class="language-js">
    // somewhere in an input config
        plugins: [
            wontache({
                delimiters: [&#39;[&#39;, &#39;]&#39;],
            }),
        ],
    </code></pre>
    <h3 id="wontache">wontache</h3>
    <p><em>String with valid JavaScript variable name</em> &mdash; <strong>Default:</strong> <code>&#39;mustache&#39;</code></p>
    <p>The transformed module imports the Mustache engine from Wontache. The <code>wontache</code> option lets you override the name by which it is imported.</p>
    <p>Transformed module, as the plugin outputs it by default:</p>
    <pre><code class="language-js">
    import mustache from &#39;wontache&#39;;
    export default mustache(...);
    </code></pre>
    <p>Your override in <code>rollup.config.js</code>:</p>
    <pre><code class="language-js">
    // somewhere in an input config
        plugins: [
            wontache({
                wontache: &#39;sideburns&#39;,
            }),
        ],
    </code></pre>
    <p>Alternative transformed module:</p>
    <pre><code class="language-js">
    import sideburns from &#39;wontache&#39;;
    export default sideburns(...);
    </code></pre>
    <script src="prism.js"></script>
</body></html>