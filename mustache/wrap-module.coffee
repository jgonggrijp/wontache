import mustache from '.'

# Expressions of the form `mustache(...)`.
templateExpression = '''
    {{&wontache}}({{&template}}{{#delimiters}}{{^precompile}}, {{&.}}{{/precompile}}{{/delimiters}})
'''

# Mustache expression wrapped as the only (default) export of a module, in three
# different module conventions.

ESM = '''
    import {{&wontache}} from 'wontache';
    export default {{>templateExpression}};
'''

AMD = '''
    define(['wontache'], function({{&wontache}}) {
        return {{>templateExpression}};
    });
'''

CommonJS = '''
    var {{&wontache}} = require('wontache');
    module.exports = {{>templateExpression}};
'''

# We wrap templates as modules using the above templates, so we need the latter
# available as partials.
partials = {templateExpression, ESM, AMD, CommonJS}

# All of the above consolidated in a single template, that dynamically selects
# the apropriate partial based on the module type.
render = mustache '{{>*type}}\n'

# Given a string of Mustache `template` code and some options, return a string
# of JavaScript code. The JavaScript code represents a complete module with a
# single (default) export, being the compiled `template`. If `precompile` is
# `true`, `template` compilation happens during the invocation of `wrapModule`,
# saving startup time in the generated JavaScript module; otherwise, compilation
# is postponed until the generated module is evaluated, saving code size. The
# `type` defaults to `'ESM'`, but can also be set to `'AMD'` or `'CommonJS'`.
# `wontache` can be overridden to set an alternative name for the compiler as it
# is imported from the Wontache package.
wrapModule = (template, {precompile, type, delimiters, wontache} = {}) ->
    template = if precompile
        mustache template, delimiters
        .source
    else
        JSON.stringify template
    if delimiters then delimiters = JSON.stringify delimiters
    type ?= 'ESM'
    wontache ?= 'mustache'
    render {template, precompile, type, delimiters, wontache}, {partials}

export default wrapModule
