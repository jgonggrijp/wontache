###
Perfectly regular Rollup configuration, plus a dash of glue at the end.

This file does roughly what you think it does (if you know rollup), but not quite in the way you might expect. Rollup will only do a regular `import`/`require` if the name of the config file ends in `.mjs` or `.cjs` and otherwise transform the file through its own API. In the latter case, passing `--require` to Node.js will do nothing to help Rollup understand CoffeeScript.

We could give this file a `.mjs` extension, but that would be a lie and it is also a bit ugly. The more truthful workaround is to invoke the Rollup API directly instead of through the CLI. Fortunately, this is only a little bit more complicated than lying about the file type.
###

import { resolve } from 'path'
import { map, defaults } from 'underscore'
import { auto, apply, asyncify } from 'async'
import coffee from 'rollup-plugin-coffee-script'
import alias from '@rollup/plugin-alias'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { babel } from '@rollup/plugin-babel'
import { bundleInputA as bundleInput } from 'wontache-devtools-rollup'

# First, a config like you'd otherwise write in a regular rollup.config.js.

extractedUnderscore = resolve __dirname, 'underscore-x.js'
nodeModuleSplit = /^.+?node_modules\/(.+)$/
unresolve = (id) -> nodeModuleSplit.exec(id)[1]

coffeePlugin = coffee()
aliasPlugin = alias
    entries: [
        {find: /^(.+?).coffee$/, replacement: '$1.js'}
    ]
nodePlugins = [nodeResolve(), commonjs()]
babelPlugin = babel
    babelHelpers: 'bundled'
    extensions: ['.coffee']
    presets: [['@babel/preset-env',
        forceAllTransforms: true
    ]]
transpilePlugins = [coffeePlugin, nodePlugins..., babelPlugin]
postTranspilePlugins = [aliasPlugin, nodePlugins...]

inputConf = (bareConf) -> defaults bareConf, {}
outputConf = (bareConf) -> defaults bareConf,
    sourcemap: true

indexCoffee = inputConf
    # Primary source of our custom Underscore index.
    input: 'customUnderscore/index.coffee'
    external: /node_modules\//
    plugins: transpilePlugins
    output: map [{
        # Published version and also input to the next conf.
        entryFileNames: '[name].js'
        dir: 'customUnderscore'
        paths: unresolve
    }], outputConf

indexJs = inputConf
    # Generated from the previous conf.
    input: 'customUnderscore/index.js'
    plugins: postTranspilePlugins
    output: map [{
        # Checking whether our custom Underscore index is correct, part 1.
        file: 'underscore-inline.js'
    }], outputConf

mustacheCoffee = inputConf
    # Primary source of our Mustache implementation.
    input: 'mustache.coffee'
    plugins: transpilePlugins
    external: /node_modules\//
    output: map [{
        # Direct translation to JS with ESM.
        # Packaged for use in custom Underscore and also input to next conf.
        file: 'module.js'
        paths: unresolve
    }], outputConf

moduleJs = inputConf
    # Generated from the previous conf.
    input: 'module.js'
    plugins: postTranspilePlugins
    output: map [{
        # Intermediate step in auto-refactoring to use monolithic Underscore.
        entryFileNames: 'mustache-pre.js'
        chunkFileNames: '[name]-x.js'
        dir: '.'
        minifyInternalExports: false
        manualChunks: (id) ->
            if id.includes 'underscore/modules' then 'underscore'
    }], outputConf

mustachePre = inputConf
    # Generated from the previous conf.
    input: 'mustache-pre.js'
    external: extractedUnderscore
    output: map [{
        # Output for browser and deno ESM consumers.
        file: 'mustache-esm.js'
        paths:
            [extractedUnderscore]: 'underscore'
    }, {
        # Output for old module system consumers.
        file: 'mustache-umd.js'
        format: 'umd'
        name: 'wontache'
        globals:
            [extractedUnderscore]: '_'
        paths:
            [extractedUnderscore]: 'underscore'
    }], outputConf

nodeWrapper = inputConf
    # Primary source of the Node.js ESM integration wrapper.
    input: 'node-wrapper.coffee'
    external: './mustache-umd.js'
    plugins: [coffeePlugin]
    output: map [{
        file: 'mustache-node.mjs'
    }], outputConf

wrapCoffee = inputConf
    # Primary source of the module wrapping module.
    input: 'wrap-module.coffee'
    external: '.'
    plugins: [coffeePlugin, babelPlugin]
    output: map [{
        # For now, we only offer a CommonJS version. Use outside of Node.js is
        # not (yet) supported and ESM mode can import from CommonJS modules just
        # fine.
        file: 'wrap-module.js'
        format: 'cjs'
        exports: 'auto'
    }], outputConf

# Now the glue.
auto
    indexCoffee: [apply bundleInput, indexCoffee]
    indexJs: ['indexCoffee', apply bundleInput, indexJs]
    mustacheCoffee: [apply bundleInput, mustacheCoffee]
    moduleJs: ['mustacheCoffee', apply bundleInput, moduleJs]
    mustachePre: ['moduleJs', apply bundleInput, mustachePre]
    nodeWrapper: ['mustachePre', apply bundleInput, nodeWrapper]
    wrapCoffee: [apply bundleInput, wrapCoffee]
.catch console.error
