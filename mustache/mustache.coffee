import isFunction from 'underscore/modules/isFunction.js';
import isNumber from 'underscore/modules/isNumber.js';
import isString from 'underscore/modules/isString.js';
import isObject from 'underscore/modules/isObject.js';
import isArray from 'underscore/modules/isArray.js';
import escape from 'underscore/modules/escape.js';

# Tokenization
# ------------
# Breaking template text into its constituent parts.

# Pattern constants for tokenization.
defaultDelimiters = ['{{', '}}']
newline = /\r?\n|\r/.source
spaces = /[ \t]*/.source

# Content of a tag. We capture all parts because we need to be able to
# reconstruct the original template text.
payload = ///(?:
    (![^]*?)                            | # comment
    (=\s*)([^=\s]+)(\s+)([^=\s]+)(\s*=) | # set delimiter
    (\{\s*)(.+?)(\s*\})                 | # unescaped variable with {{{}}}
    ([&\#^/><$]?)(\s*)(.+?)(\s*)          # regular key-based tags
)///.source

# Characters that must be escaped inside delimiters.
specialChars = /[(){}[\]^$\.*+?\\|]/g

# Escaped version of a regular expression.
escapeRegex = (string) -> string.replace specialChars, '\\$&'

# Tokenization. Resulting stream always starts with a literal.
tokenize = (string, delimiters) ->
    open = escapeRegex delimiters[0]
    close = escapeRegex delimiters[1]
    string.split new RegExp "(#{newline})?(#{spaces})(#{open})#{payload
        }(#{close})(#{spaces}(?:#{newline}))?"

# Total number of capturing groups per tag, plus one for the literal in between.
stride = 19

# Safe concatenation for compile-time, when captured tokens might be undefined.
concat = (stream, start, end) ->
    (t for t in stream.slice(start, end) when t).join('')

# Re-tokenize the trailing part of a previously tokenized stream, using the new
# delimiters.
retokenize = (stream, position, delimiters) ->
    trailing = concat stream, position
    retokenized = tokenize trailing, delimiters
    stream.splice.apply stream, [position, stream.length].concat retokenized

# Compare a pair of delimiters to the default.
areDefault = (delim) ->
    return delim[0] is defaultDelimiters[0] and delim[1] is defaultDelimiters[1]

# Parsing
# -------
# Turning a flat sequence of constituents into a hierarchy. This stage is purely
# syntactical; we focus exclusively on collecting enough information to identify
# which tags belong together and whether they are standalone or not. The literal
# text between the tags is left implicit.

# Mnemonics for tag types. Reusing sigils for code economy.
VARIABLE = ''
UNESCAPED = '&'
SECTION = '#'
INVERTED = '^'
CLOSE = '/'
PARTIAL = '>'
PARENT = '<'
BLOCK = '$'
COMMENT = '!'
SET_DELIMITER = '='

# Determine whether a tag is cleared by a line end on either side.
detectClearance = (stream, oldPosition, position, slice) ->
    prepost = if stream[oldPosition - 1] then true else oldPosition is 0
    clearLeft = slice[1]? or slice[0] is '' and prepost
    clearRight = slice[18]? or position + 1 is stream.length and slice[19] is ''
    {clearLeft, clearRight}

# Nesting errors encountered during parsing.
strayEndingTag = (stream, tag) ->
    throw SyntaxError "Stray section end: #{concat stream, tag.start, tag.end}"
sectionMismatch = (stream, open, close) ->
    begin = concat stream, open.start, open.end
    end = concat stream, close.start, close.end
    throw SyntaxError "Nonmatching keys at begin and end of section:
        #{begin}...#{end}"
unclosedSection = (stream, tag) ->
    throw SyntaxError "No matching section end found before end of template:
        #{concat stream, tag.start, tag.end}"

# Core parsing algorithm. Recurses into sections, parents and blocks.
parse = (stream, position, delimiters, inParent) ->
    tags = []
    loop
        oldPosition = position
        position += stride
        break if position > stream.length
        # [,, indent,,,, open,, close,,, altKey,, sigil,, key,,,,]
        slice = stream.slice oldPosition, position + 1
        sigil = slice[13]
        altKey = slice[11]
        # For every tag in the template, we collect basic information such as
        # its `key` and `type`, its boundaries and the `delimiters` that were in
        # effect immediately *before* the tag.
        tagInfo = detectClearance stream, oldPosition, position, slice
        tagInfo.start = oldPosition + 3
        tagInfo.end = position - 1
        tagInfo.key = key = altKey or slice[15]
        tagInfo.delimiters = delimiters
        tagInfo.type = type = if altKey then UNESCAPED
        else if key then sigil
        else if open = slice[6]
            close = slice[8]
            if open isnt delimiters[0] or close isnt delimiters[1]
                delimiters = [open, close]
                retokenize stream, position, delimiters
            SET_DELIMITER
        else COMMENT
        if type is CLOSE
            # End a recursive call.
            closingTag = tagInfo
            break
        continue if inParent and type isnt BLOCK
        tags.push switch type
            when SECTION, INVERTED, PARENT, BLOCK
                # Start a recursive call.
                nested = parse stream, position, delimiters, type is PARENT
                {position, delimiters, closingTag: close} = nested
                if not close then unclosedSection stream, tagInfo
                if close.key isnt tagInfo.key
                    sectionMismatch stream, tagInfo, close
                # The object that we represent a subtree with is somewhat
                # similar to a `tagInfo`: both have a `key` and a `type`. The
                # difference is that the boundaries are represented through the
                # `open` and `close` properties and that it contains an array of
                # nested `tags`.
                {type, key, open: tagInfo, close, tags: nested.tags}
            else tagInfo
    {delimiters, position, tags, closingTag}

# Runtime support functions
# -------------------------
# These will be used inside the generated functions. Where the parsing section
# expressed the syntax of the template language, the current section expresses
# its semantics.

# Use the section function to "render" the original template text.
decompile = (state, sectionFunc) ->
    state.d = true
    text = sectionFunc state
    state.d = false
    text

# Resolve a name in the context, following the algorithm from the spec.
resolve = (state, keys, sectionFunc) ->
    keyIndex = 0
    key = keys[keyIndex]
    size = keys.length
    stack = state.context
    end = stack.length - 1
    if size
        for frameIndex in [end..0] by -1
            frame = stack[frameIndex]
            break if frame? and (value = frame[key]) isnt undefined
        if value is undefined then frame = undefined
    else
        frame = stack[end - 1]
        value = stack[end]
    while frame? and ++keyIndex < size
        frame = if isFunction value
            if sectionFunc
                sectionFunc.sectionText ?= decompile state, sectionFunc
                value.call frame, sectionFunc.sectionText
            else value.call frame
        else value
        value = frame?[keys[keyIndex]]
    {frame, value}

# Substitute a `{{variable}}`, `{{{variable}}}` or `{{&variable}}`.
interpolate = (state, key, safe = true) ->
    {frame, value} = resolve state, key
    if isFunction value
        if isString value = value.call frame
            value = mustache(value)(null, state)
    if safe then escape value else String value ? ''

# Render a `{{#section}}...{{/section}}` zero or more times.
renderSection = (state, key, func, delimiters) ->
    func = state.sections[func]
    if state.d then return func state
    {frame, value} = resolve state, key, func
    return '' unless value
    if isFunction value
        sectionText = func.sectionText ?= decompile state, func
        value = value.call frame, sectionText
        if value is sectionText then return func state
        if isString value then return mustache(value, delimiters)(null, state)
    if isString(value) or not isNumber value.length then value = [value]
    repeats = ''
    {context} = state
    for item in value
        context.push item
        repeats += func state
        context.pop()
    repeats

# Render a `{{^section}}...{{/section}}` zero or one times.
renderInvertedSection = (state, key, func) ->
    func = state.sections[func]
    if state.d then return func state
    {value} = resolve state, key, func
    switch
        when not value then func state
        when isFunction value then ''
        when value.length is 0 then func state
        else ''

# Indent a block of text.
lineBreak = /^(?!$)/mg
addIndentation = (text, indent) -> text.replace lineBreak, indent

# Render a `{{>partial}}`, `{{<parent}}` or `{{$block}}`, precompiling and
# indenting as needed.
renderSub = (state, name, control, indent) ->
    {partials, blocks, sections} = state
    if isNumber control
        # Numeric index of a block.
        template = not state.d and blocks[name] or sections[control]
        control = blocks
    else
        # Partial or parent.
        if isArray name # Dynamic partial.
            name = interpolate state, name, false
        template = partials[name]
        if isString template
            text = template
            partials[name] = template = mustache template
            template.templateText = text
        return '' unless isFunction template
        if isObject control
            # Parent.
            control[key] = sections[index] for key, index of control
            control[key] = blocks[key] for key of blocks
        else
            # Plain partial, static or dynamic.
            indent = control
            control = {}
    options = {context: state.context, partials, blocks: control}
    if indent
        reindented = template.reindented ?= {}
        if reindented[indent]
            template = reindented[indent]
        else
            text ?= template.templateText ?= template null, decompile: true
            text = addIndentation text, indent
            template = reindented[indent] = mustache text, template.delimiters
            template.templateText = text
    if state.d
        text ? template.templateText ?= template null, decompile: true
    else template null, options

# Code generation
# ---------------
# Where new JavaScript functions are born.

# Compiled representation of a (dotted) name or implicit iterator.
splitKey = (key) -> if key is '.' then '[]' else JSON.stringify key.split '.'

# Argument list for a function call, from a list of compiled expressions.
argumentList = (args) -> "(state, #{args.join ', '})"

# Expressions representing each of the main constituents of a Mustache template.
# We compose larger string concatenation expressions out of these.
# Literal parts are simply represented as themselves using `JSON.stringify`.
variable = (args) -> "state.v#{argumentList args}"
section = (type, args, delimiters) ->
    args.push JSON.stringify delimiters unless areDefault delimiters
    "state.#{if type is SECTION then 's' else 'z'}#{argumentList args}"
partial = (args) -> "state.p#{argumentList args}"

# In decompile mode, tags should render as themselves instead of as whatever
# their semantics dictate.
conditionalTag = (decompiling, compiling = '""') ->
    "(state.d ? #{JSON.stringify decompiling} : #{compiling})"

# Each section gets its own function with a string concatenation expression.
sectionFunc = (terms) -> """
    function(state) {
        return #{terms.join(' + ') or '""'};
    }
"""

# Array of section functions.
sectionList = (funcs) -> "[#{funcs.join ', '}]"

# Full description of both whole templates and nested blocks.
templateObject = (sectionList, delimiters) ->
    parts = ["sections: #{sectionList}"]
    unless areDefault delimiters
        parts.push "delimiters: #{JSON.stringify delimiters}"
    "{#{parts.join ', '}}"

# Literal parts of the template are copied verbatim, but for efficiency, we only
# do this when not empty.
pushNonEmptyLiteral = (stream, start, end, terms) ->
    if literalFragment = concat stream, start, end
        terms.push JSON.stringify literalFragment

# If a tag clears on both sides, consider leading indentation and trailing
# spaces and line end part of the tag text in order to prevent spurious
# whitespace in the output. We allow passing a second argument so that clearing
# and position information about the left and right sides can be obtained from
# different tags.
correctStandalone = (leftTag, {end, clearRight} = leftTag) ->
    {start, clearLeft} = leftTag
    isStandalone = (clearLeft and clearRight)
    start -= +isStandalone
    end += +isStandalone
    {start, end, isStandalone}

# Mnemonic for the character that starts dynamic names, and a regular
# expression for taking it off, together with following whitespace.
DYNAMIC = '*'
derefOp = /^\*\s*/

# In partials and parents, the name may be prefixed with a star to indicate that
# the name should be dynamically resolved from the context. In that case, the
# star is removed and the remainder of the name is split on the dots, like in
# variables and sections. Otherwise, we encode the name as a single string.
possiblyDynamic = (key) ->
    if key[0] is DYNAMIC
        splitKey key.replace derefOp, ''
    else
        JSON.stringify key

# Generate code for the content within a section of the template, pushing one or
# more function source strings to `sections`. This is also the entry point for
# the template as a whole. Returns the index of the top-level generated section.
generateSection = (stream, previousEnd, nextStart, {tags}, sections) ->
    terms = []
    for tree in tags
        term = renderTerm = undefined
        {type, key, start, end, open, close} = tree
        if type is PARENT or type is BLOCK
            {start, end, isStandalone} = correctStandalone open, close
            indent = isStandalone and stream[start]
        switch type
            when VARIABLE, UNESCAPED
                args = [splitKey key]
                if type is UNESCAPED then args.push 0
                renderTerm = variable args
            when SECTION, INVERTED
                {start, end: iEnd} = correctStandalone open
                {start: iStart, end} = correctStandalone close
                serial = generateSection stream, iEnd, iStart, tree, sections
                {delimiters} = open
                renderTerm = section type, [splitKey(key), serial], delimiters
            when PARTIAL
                {start, end, isStandalone} = correctStandalone tree
                args = [possiblyDynamic key]
                if isStandalone then args.push JSON.stringify stream[start]
                renderTerm = partial args
            when PARENT
                term = generateParent stream, start, end, tree, sections, indent
            when BLOCK
                if isStandalone and not close.clearLeft then --end
                blockInfo = generateBlock stream, tree, sections, indent
                {iEnd, iStart, renderTerm} = blockInfo
            else
                {start, end} = correctStandalone tree
        if type is SECTION or type is INVERTED or type is BLOCK
            term = [
                conditionalTag concat stream, start, iEnd
                renderTerm
                conditionalTag concat stream, iStart, end
            ].join ' + '
        pushNonEmptyLiteral stream, previousEnd, start, terms
        unless term
            tagFragment = concat stream, start, end
            term = conditionalTag tagFragment, renderTerm
        terms.push term
        previousEnd = end
    pushNonEmptyLiteral stream, previousEnd, nextStart, terms
    sections.push(sectionFunc terms) - 1

# Within `{{<parent}}...{{/parent}}` tags, all text outside of blocks is
# considered a comment. Whitespace is handled quite differently as a result.
generateParent = (stream, start, end, tree, sections, indent) ->
    terms = []
    blocks = {}
    for subtree in tree.tags
        blockInfo = generateBlock stream, subtree, sections
        blocks[subtree.key] = blockInfo.serial
        pushNonEmptyLiteral stream, start, blockInfo.iEnd, terms
        terms.push blockInfo.renderTerm
        start = blockInfo.iStart
    pushNonEmptyLiteral stream, start, end, terms
    args = [possiblyDynamic(tree.key), JSON.stringify(blocks)]
    if indent then args.push JSON.stringify indent
    "(state.d ? #{terms.join ' + '} : state.p#{argumentList args})"

# Remove intrinsic indentation from a block. We re-tokenize and re-parse the
# stream, because doing this correctly is otherwise extremely complicated.
initialIndent = new RegExp "^#{spaces}"
removeIndentation = (stream, {open, close}) ->
    start = open.end + 1
    end = close.start - 2
    text = concat stream, start, end
    indent = initialIndent.exec(text)[0]
    if not indent then return indent
    dedentPattern = new RegExp "^#{indent}", 'mg'
    chunks = tokenize text.replace(dedentPattern, ''), open.delimiters
    parse chunks, 0, open.delimiters
    stream.splice.apply stream, [start, end - start].concat chunks
    indent

# A block behaves like a partial that is embedded within the larger template and
# that can be exchanged with other templates. For this reason, most of the code
# generation happens inside a recursive call to generateSection.
generateBlock = (stream, tree, sections, indent) ->
    {open, close} = tree
    iEnd = open.end
    iStart = close.start
    nonempty = iStart - iEnd > 4 or stream[iEnd + 1]
    if indent isnt false and open.clearRight and nonempty
        # Intrinsic indentation of the content between the block tags takes
        # precedence over any indentation that might be in front of the open
        # tag. This is done at code generation time because we need to make a
        # preorder traversal of nested blocks.
        indent = removeIndentation stream, tree
        iEnd += 1
        iStart -= close.clearLeft
    subSections = []
    generateSection stream, iEnd, iStart, tree, subSections
    serial = sections.length
    sections.push templateObject sectionList(subSections), open.delimiters
    args = [JSON.stringify(tree.key), serial]
    if indent then args.push JSON.stringify indent
    renderTerm = partial args
    {serial, renderTerm, indent, iEnd, iStart}

# Wrapping up
# -----------
# We compose our final interface from the parts above.

# The function this is all about, and which we will export.
# One stop solution for compiling and wrapping templates.
mustache = (source, delimiters = defaultDelimiters) ->
    # Compile the template text if this hasn't been done yet.
    if isString source
        stream = tokenize source, delimiters
        tree = parse stream, 0, delimiters
        if tree.closingTag then strayEndingTag stream, tree
        sections = []
        generateSection stream, 0, stream.length, tree, sections
        sectionText = sectionList sections
        sections = new Function("return #{sectionText}")()
    else
        {sections, delimiters} = source
        delimiters ?= defaultDelimiters
    unless isArray sections
        throw new TypeError "Expected template string or precompiled template
            representation, got #{source}"
    # Recurse into any nested blocks if present.
    for element, index in sections
        unless isFunction element
            sections[index] = mustache element
    # Create the final template function out of the compiled section functions.
    # Fun fact: this is the only closure in the whole setup.
    topLevel = sections.pop()
    template = (data, {context, partials, blocks, decompile} = {}) ->
        topLevel
            context: context or [data]
            sections: sections
            partials: partials or mustache.partials
            blocks: blocks or {}
            d: decompile
            v: interpolate
            s: renderSection
            z: renderInvertedSection
            p: renderSub
    template.delimiters = delimiters
    if sectionText
        template.source = templateObject sectionText, delimiters
    template

mustache.partials = {}

export default mustache
