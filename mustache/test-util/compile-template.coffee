import { readFile, writeFile } from 'fs'
import { resolve } from 'path'
import { waterfall, apply, asyncify } from 'async'
import mustache from '../mustache.coffee'

templateFile = resolve __dirname, 'example-template.mustache'
outputFile = resolve __dirname, 'example-template.js'

# This is just for our test setup. The compiled template uses the CommonJS
# module format because it is a JS file and we told Babel to only transform CS
# files.
moduleTemplate = mustache '''
    const mustache = require('../mustache.coffee').default;
    module.exports = mustache({{&compiled}});

'''

# Read, compile, wrap, write.
# It looks a bit alien because of async, but there's nothing special here.
waterfall [
    apply readFile, templateFile, 'utf8'
    asyncify (templateText) -> mustache(templateText).source
    asyncify (compiled) -> moduleTemplate {compiled}
    apply writeFile, outputFile
]
.catch console.error
