import assert from 'assert'

import mustache from './mustache.coffee'
import wrapModule from './wrap-module.coffee'

rawTemplate = 'Hello, {{name}}!'
precompiled = mustache(rawTemplate).source

rawDelimiters = 'Hello, <name>!'
delimiters = ['<', '>']
delimitersJSON = JSON.stringify delimiters
preDelimiters = mustache(rawDelimiters, delimiters).source

describe 'wrapModule', ->
    it 'wraps a template as an ESM module', ->
        assert wrapModule(rawTemplate) is """
            import mustache from 'wontache';
            export default mustache("#{rawTemplate}");
        """

    it 'takes alternative delimiters into account', ->
        assert wrapModule(rawDelimiters, {delimiters}) is """
            import mustache from 'wontache';
            export default mustache("#{rawDelimiters}", #{delimitersJSON});
        """

    it 'supports precompilation', ->
        assert wrapModule(rawTemplate, {precompile: true}) is """
            import mustache from 'wontache';
            export default mustache(#{precompiled});
        """

    it 'supports alternative names for mustache', ->
        assert wrapModule(rawTemplate, {wontache: 'Einstein'}) is """
            import Einstein from 'wontache';
            export default Einstein("#{rawTemplate}");
        """

    it 'supports alternative module formats', ->
        assert wrapModule(rawTemplate, {type: 'AMD'}) is """
            define(['wontache'], function(mustache) {
                return mustache("#{rawTemplate}");
            });
        """

        assert wrapModule(rawTemplate, {type: 'CommonJS'}) is """
            var mustache = require('wontache');
            module.exports = mustache("#{rawTemplate}");
        """

    it 'supports all options at the same time', ->
        result1 = wrapModule rawDelimiters,
            delimiters: delimiters
            precompile: true
            wontache: 'Nietzsche'
            type: 'AMD'
        assert result1 is """
            define(['wontache'], function(Nietzsche) {
                return Nietzsche(#{preDelimiters});
            });
        """

        result2 = wrapModule rawDelimiters,
            delimiters: delimiters
            wontache: 'Khan'
            type: 'CommonJS'
        assert result2 is """
            var Khan = require('wontache');
            module.exports = Khan("#{rawDelimiters}", #{delimitersJSON});
        """
