import assert from 'assert'
import * as fs from 'fs'
import * as path from 'path'

import { isFunction, isString, each, filter, map, zip }  from 'underscore'
import * as a$ from 'async'
import YAML from 'yaml'
import { parseMap }  from 'yaml/util'

import mustache from './mustache.coffee'
import compiledTemplate from './test-util/example-template.js'

pathToSpecs = path.resolve __dirname, 'spec', 'specs'

codeTag =
    tag: '!code'
    identify: (value) -> isFunction value
    resolve: (doc, cst) ->
        new Function("return #{parseMap(doc, cst).get 'js'};")()

YAML.defaultOptions.customTags = [codeTag]

yamlFileNames = a$.waterfall [
    a$.apply fs.readdir, pathToSpecs
    a$.asyncify (files) ->
        filter files, (f) ->
            f.endsWith '.yml'
]

# Take the above names of .yml specs, compute their full paths, read the file
# contents, zip those contents together with the original base names, and
# finally generate a Mocha suite based on that.
a$.waterfall [
    a$.asyncify -> yamlFileNames
    a$.asyncify (fileNames) ->
        map fileNames, (f) ->
            fullPath = path.resolve pathToSpecs, f
            a$.apply fs.readFile, fullPath, 'utf8'
    (readFiles, cb) -> a$.parallel readFiles, cb
    a$.asyncify (yamlFiles) ->
        yamlFileNames.then (fileNames) ->
            zip fileNames, yamlFiles
], (error, specs) ->
    if error then return console.error error

    describe 'official mustache specification', ->
        partials = null
        beforeEach -> partials = mustache.partials
        afterEach -> mustache.partials = partials

        each specs, ([specName, yamlText]) ->
            describe specName, ->
                specData = YAML.parse(yamlText).tests
                each specData, ({name, partials, template, data, expected}) ->
                    it name, ->
                        assert mustache(template)(data, {partials}) is expected

    run()

describe 'mustache core API and syntax', ->
    beforeEach ->
        @nested = y: 1, x: y: 2, x: y: 3
        assert @nested.x.x.y is 3
        @oldPartials = mustache.partials
        mustache.partials =
            boldX: mustache '<b>{{x}}</b>'
            italicBoldX: mustache '<i>{{>boldX}}</i>'
            deepY: mustache '{{#x}}{{>deepY}}{{/x}}{{^x}}{{y}}{{/x}}'

    afterEach ->
        mustache.partials = @oldPartials

    it 'compiles templates', ->
        compiled = mustache ''
        assert isFunction compiled
        assert compiled() is ''
        source = compiled.source
        assert isString source
        assert source.startsWith '{'
        assert source.endsWith '}'

    describe 'precompilation', ->
        before (done) ->
            dir = path.resolve __dirname, 'test-util'
            name = path.resolve dir, 'example-template.mustache'
            fs.readFile name, 'utf8', (error, text) =>
                if error then return done(error)
                @templateText = text
                done()

        it 'produces code that can be written to a file and imported later', ->
            # Note that compiledTemplate is imported at the top of this module.
            # It is the precompiled version of the example-template.mustache
            # that we read from the filesystem above.
            assert isFunction compiledTemplate

        it 'produces code that is equivalent to the "live" template', ->
            data =
                x: 'coffee & mustaches'
                a: b: [
                    {x: 'tea & eyebrows'}
                    {x: 'mate & sideburns'}
                ]
                c: z: 12
            expected = ' \
                * coffee &amp; mustaches\n
                * \n
                * coffee & mustaches\n
                * tea & eyebrows\n        <b>tea &amp; eyebrows</b>\n
                * 12\n
                * mate & sideburns\n        <b>mate &amp; sideburns</b>\n
                * 12\n
            '
            assert mustache(@templateText)(data) is expected
            assert compiledTemplate(data) is expected

        it 'produces code with a backdoor to get the original template', ->
            getSource = {decompile: true}
            assert compiledTemplate(null, getSource) is @templateText
            assert mustache(@templateText)(null, getSource) is @templateText

    variablesCommon = ->
        it 'get interpolated', ->
            assert @render() is 'abc  def'
            assert @render(x: null) is 'abc  def'
            assert @render(x: 'banana') is 'abc banana def'
            assert @render(x: 10) is 'abc 10 def'

    describe 'variables', ->
        before ->
            @template = 'abc {{x}} def'
            @render = mustache @template

        variablesCommon()

        it 'are escaped', ->
            assert @render(x: '<script>') is 'abc &lt;script&gt; def'

    unescapedVariablesCommon = ->
        it 'are not escaped', ->
            assert @render(x: '<script>') is 'abc <script> def'

    describe 'unescaped variables with triple mustaches', ->
        before ->
            @template = 'abc {{{x}}} def'
            @render = mustache @template

        variablesCommon()
        unescapedVariablesCommon()

    describe 'unescaped variables with indicator', ->
        before ->
            @template = 'abc {{&x}} def'
            @render = mustache @template

        variablesCommon()
        unescapedVariablesCommon()

    describe 'comments', ->
        it 'are not rendered', ->
            @render = mustache 'abc {{!ignore}} def'
            assert @render() is 'abc  def'
            assert @render(ignore: 'banana') is 'abc  def'

        it 'can span multiple lines', ->
            @render = mustache '''
                abc {{!
                    if you could be so kind to ignore this
                }} def
            '''
            assert @render() is 'abc  def'

        it 'may contain anything except for the close delimiter', ->
            @render = mustache 'abc {{!}{{#{{^{{/{{name}{{!{{>!@#$%^&*(}} def'
            assert @render() is 'abc  def'

    describe 'sections', ->
        beforeEach ->
            @render = mustache 'abc {{#x}} def {{y}} ghi {{/x}} jkl'

        it 'descend into nested contexts', ->
            assert @render(@nested) is 'abc  def 2 ghi  jkl'
            @render = mustache """
                {{y}}{{#x}}{{y}}{{#x}}{{y}}{{/x}}{{y}}{{/x}}{{y}}
            """
            assert @render(@nested) is '12321'

        it 'produce no output with falsy keys', ->
            assert @render(y: 2) is 'abc  jkl'
            assert @render(x: null, y: 2) is 'abc  jkl'
            assert @render(x: false, y: 2) is 'abc  jkl'
            assert @render(x: '', y: 2) is 'abc  jkl'

        it 'repeat for every element in a list', ->
            assert @render(x: [], y: 2) is 'abc  jkl'
            assert @render(x: [{y: 1}], y: 2) is 'abc  def 1 ghi  jkl'
            assert @render(x: [{y: 1}, {y: 2}, {y: 3}]) is """
                abc  def 1 ghi  def 2 ghi  def 3 ghi  jkl
            """

        it 'pass through function keys', ->
            x = (text) -> "(#{text})"
            x.y = 2
            assert @render({x, y: 1}) is 'abc ( def 1 ghi ) jkl'

        it 'allow function keys to override the template', ->
            x = (text) -> '[[{{y}}]]'
            assert @render({x, y: 1}) is 'abc [[1]] jkl'

        it 'allow function keys to skip rendering entirely', ->
            x = (text, render) -> 'CENSORED'
            assert @render({x, y: 1}) is 'abc CENSORED jkl'

        it 'accept new stack frames from function keys', ->
            x = -> y: 2
            assert @render({x, y: 1}) is 'abc  def 2 ghi  jkl'

    describe 'inverted sections', ->
        beforeEach ->
            @render = mustache 'abc {{^x}} def {{y}} ghi {{/x}} jkl'

        it 'don\'t descend into nested contexts', ->
            assert @render(x: {}, y: 1) is 'abc  jkl'
            assert @render(@nested) is 'abc  jkl'

        it 'do render for falsy keys', ->
            assert @render(y: 2) is 'abc  def 2 ghi  jkl'
            assert @render(x: null, y: 2) is 'abc  def 2 ghi  jkl'
            assert @render(x: false, y: 2) is 'abc  def 2 ghi  jkl'
            assert @render(x: '', y: 2) is 'abc  def 2 ghi  jkl'

        it 'render lists only if empty', ->
            assert @render(x: [], y: 2) is 'abc  def 2 ghi  jkl'
            assert @render(x: [{y: 1}], y: 2) is 'abc  jkl'
            assert @render(x: [{y: 1}, {y: 2}, {y: 3}], y: 2) is 'abc  jkl'

        it 'skip functions', ->
            x = (text, render) -> "(#{render text})"
            x.y = 2
            assert @render({x, y: 1}) is 'abc  jkl'

    describe 'partials', ->
        it 'are rendered inline', ->
            @render = mustache 'abc {{>boldX}} def'
            assert @render(x: 'banana') is 'abc <b>banana</b> def'

        it 'may invoke other partials', ->
            @render = mustache 'abc {{>italicBoldX}} def'
            assert @render(x: 'banana') is 'abc <i><b>banana</b></i> def'

        it 'indent correctly', ->
            mustache.partials.complex = mustache '''
                {{#last_name}}
                {{last_name}}{{#first_name}}, {{/first_name}}{{^first_name}}<br>{{/first_name}}
                {{/last_name}}

            '''
            template = '>\n    {{>complex}}'
            data = {first_name: 'Jane', last_name: 'Coder'}
            assert mustache(template)(data) is '>\n    Coder, \n'

        it 'reindentation is stateless', ->
            partial = 'X\n'
            template = '''
                >
                    {{>partial}}
                        {{>partial}}
            '''
            expected = '''
                >
                    X
                        X

            '''
            assert mustache(template)({}, partials: {partial}) is expected

        it 'carry over across lambda sections', ->
            partial = '[{{x}}]'
            template = '{{#lambda}}{{>partial}}{{/lambda}}'
            data =
                x: 10
                lambda: (section) -> section + '.'
            expected = '[10].'
            assert mustache(template)(data, partials: {partial}) is expected

        it 'respect delimiters after reindentation', ->
            customDelimiters = ['[', ']']
            partial = mustache 'Hello, [name].', customDelimiters
            template = mustache '''
                I say:
                    [>partial]
            ''', customDelimiters
            expected = '''
                I say:
                    Hello, Jane.
            '''
            assert template({name: 'Jane'}, partials: {partial}) is expected

    describe 'parents', ->
        it 'decompile correctly', ->
            template = '{{<parent}}a{{$block}}b{{/block}}c{{/parent}}'
            assert mustache(template)(null, decompile: true) is template

    describe 'dynamic partials', ->
        it 'work', ->
            template = '{{>*dynamic}}'
            partial = 'Hello'
            data = dynamic: 'partial'
            expected = 'Hello'
            assert mustache(template)(data, partials: {partial}) is expected

        it 'decompile correctly', ->
            template = '{{>*dynamic}}'
            assert mustache(template)({}, decompile: true) is template

        it 'can also be parents', ->
            template = '{{<*dynamic}}{{$block}}Hi{{/block}}{{/*dynamic}}'
            parent = '{{$block}}Hello{{/block}}'
            data = dynamic: 'parent'
            expected = 'Hi'
            render = mustache template
            assert render(data, partials: {parent}) is expected
            assert render(null, decompile: true) is template

        it 'can use dotted names and implicit iterators', ->
            template = '{{>*here.I.am}}'
            partial = 'Hello'
            data = here: I: am: 'partial'
            expected = 'Hello'
            assert mustache(template)(data, partials: {partial}) is expected
            template = '{{>*.}}'
            data = 'partial'
            assert mustache(template)(data, partials: {partial}) is expected

    describe 'blocks', ->
        it 'carry over across lambda sections', ->
            partials =
                grandParent: '{{$block}}[{{x}}]{{/block}}'
                parent: '{{#lambda}}{{<grandParent}}{{/grandParent}}{{/lambda}}'
            template = '{{<parent}}{{$block}}-{{x}}-{{/block}}{{/parent}}'
            data =
                x: 10
                lambda: (section) -> section + '.'
            expected = '-10-.'
            assert mustache(template)(data, {partials}) is expected

        it 'respect delimiters after reindentation', ->
            template = mustache '''
                {{=[ ]=}}
                [$block]
                    Hi [name]!
                [/block]
            '''
            expected = '    Hi John!\n'
            assert template(name: 'John') is expected

        it 'preserve newlines if not standalone', ->
            template = mustache '''
                x {{$b}}
                y
                {{/b}}
            '''
            expected = 'x \ny\n'
            assert template() is expected

    describe 'set delimiter', ->
        it 'takes effect', ->
            @render = mustache '{{=[ ]=}}[!comment]{{!oops}}'
            assert @render() is '{{!oops}}'

        it 'is repeatable', ->
            @render = mustache """
                {{=[ ]=}}[!a]{{!a}}[=((( )))=](((!b)))[!b](((={{ }}=))){{!c}}(((!c)))
            """
            assert @render() is '{{!a}}[!b](((!c)))'

        it 'supports variables', ->
            @render = mustache '{{=[ ]=}}[x]'
            assert @render(x: 10) is '10'
            assert @render(x: '<script>') is '&lt;script&gt;'

        it 'supports unescaped variables', ->
            @render = mustache '{{=[ ]=}}[&x]'
            assert @render(x: 10) is '10'
            assert @render(x: '<script>') is '<script>'

        it 'supports sections', ->
            @render = mustache '{{=[ ]=}}[#x][y][/x]'
            assert @render(@nested) is '2'

        it 'supports inverted sections', ->
            @render = mustache '{{=[ ]=}}[^x][y][/x]'
            assert @render(y: 2) is '2'

        it 'supports partials', ->
            @render = mustache '{{=[ ]=}}[>boldX]'
            assert @render(x: 10) is '<b>10</b>'
