# This may seem completely pointless, but it enables us to create an ESM build
# specifically for Node.js that shares state with CommonJS consumers.
export { default } from './mustache-umd.js'
