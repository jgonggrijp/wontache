export { default as isFunction } from 'underscore/modules/isFunction.js'
export { default as isNumber } from 'underscore/modules/isNumber.js'
export { default as isObject } from 'underscore/modules/isObject.js'
export { default as keys } from 'underscore/modules/keys.js'
export { default as extendOwn,
         default as assign    } from 'underscore/modules/extendOwn.js'
export { default as create } from 'underscore/modules/create.js'
export { default as isString } from 'underscore/modules/isString.js'
export { default as escape } from 'underscore/modules/escape.js'
