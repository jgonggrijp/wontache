# Wontache project

[Wontache][website] is a [Mustache][mustache] implementation for JavaScript. The Wontache project consists of multiple packages, which are kept together in a single Git repository ("monorepo"). To review the README of a specific subdirectory, click on one of the links in the table below.

| subdirectory | package name | description |
|---|---|---|
| [`comparison`][comparison-dir] | `mustache-engine-comparison` | Tools for comparing Wontache with other template engines. |
| [`mustache`][mustache-dir] |  `wontache` | Core Mustache engine. |
| [`playground`][playground-dir] | `@wontache/playground` | Source code of the online [playground][playground]. |
| [`plugins/*`][plugins-dir] | varies | Integrations for other libraries and tools. |
| [`public`][public-dir] | &mdash; | The contents of our statically generated [website][website]. |
| [`ruby-mustache`][ruby-dir] | &mdash; | Our fork of the original Mustache implementation for Ruby, included as a submodule. We use this to publish an [up-to-date version][manpage] of the [mustache(5) manpage)][old-manpage]. |
| [`tools/*`][tools-dir] | `wontache-devtools-*` | Internal development tools. |

[website]: https://jgonggrijp.gitlab.io/wontache/
[playground]: https://jgonggrijp.gitlab.io/wontache/playground.html
[manpage]: https://jgonggrijp.gitlab.io/wontache/mustache.5.html
[mustache]: https://mustache.github.io/
[old-manpage]: https://mustache.github.io/mustache.5.html
[mustache-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/mustache
[plugins-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/plugins
[tools-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/tools
[comparison-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/comparison
[playground-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/playground
[ruby-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/ruby-mustache
[public-dir]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/public


## Development

See the [CONTRIBUTING.md][contributing] for details on how to contribute.

[contributing]: https://gitlab.com/jgonggrijp/wontache/-/blob/integration/CONTRIBUTING.md
