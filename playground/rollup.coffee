import wontache from 'rollup-plugin-wontache'
import coffee from 'rollup-plugin-coffee-script'
import alias from '@rollup/plugin-alias'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import inject from '@rollup/plugin-inject'
import { babel } from '@rollup/plugin-babel'
import { terser } from 'rollup-plugin-terser'
import { bundleInput } from 'wontache-devtools-rollup'
import nollupDevServer from 'nollup/lib/dev-server'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

languagePlugins = [
    wontache precompile: true
    coffee()
]
aliasPlugin = alias entries: [
    find: /.*jquery.*/
    replacement: './jquery/dist/jquery.js'
]
otherPlugins = [
    nodeResolve
        dedupe: ['underscore']
    commonjs()
    inject
        include: 'node_modules/backbone-fractal/**/*.js'
        include: '../node_modules/backbone-fractal/**/*.js'
        'window._': 'underscore'
        'window.Backbone': 'backbone'
]
babelPlugin = babel
    babelHelpers: 'bundled'
    extensions: ['.coffee']
    presets: [['@babel/preset-env',
        forceAllTransforms: true
    ]]

nollupPlugins = languagePlugins.concat otherPlugins
bundlePlugins = languagePlugins.concat aliasPlugin, otherPlugins, babelPlugin

bundleConfig = (nollup) ->
    input: 'src/main.coffee'
    plugins: if nollup then nollupPlugins else bundlePlugins
    output:
        file: '../public/playground.js'
        format: 'iife'
        plugins: if nollup then [] else [terser()]
        sourcemap: true

main = (argv) ->
    if argv.nollup
        nollupDevServer
            config: bundleConfig true
            contentBase: '../public'
            port: 8080
    else
        bundleInput bundleConfig false
        .catch console.error

yargs hideBin process.argv
.command '$0', 'Build the playground JavaScript bundle', {}, main
.option 'nollup',
    alias: 'n'
    boolean: true
    description: 'Run a development server with Nollup
        instead of generating a static file'
.help()
.alias 'help', 'h'
.parse()
