import { CompositeView } from 'backbone-fractal'

import CodeFieldView from './code-field-view.coffee'
import dataFormTemplate from './data-form.mustache'

export default class DataFormView extends CompositeView
    tagName: 'form'
    template: dataFormTemplate
    subviews: ['editor']

    initialize: ->
        @editor = new CodeFieldView model: @model
        @render()

    renderContainer: ->
        @$el.html @template()
        @

    activate: ->
        @forEachSubview (view) -> view.activate()
