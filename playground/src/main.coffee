import { isObject, isArray } from 'underscore'
import { Collection, $ } from 'backbone'
import mustache from 'wontache'

import { defaultData, defaultTemplates } from './examples.coffee'
import channel from './radio.coffee'
import DataModel from './data-model.coffee'
import DataForm from './data-form.coffee'
import TemplateCollectionView from './template-collection-view.coffee'
import PartialManager from './partial-manager.coffee'

# In this module, all the magic from our units is going to come together. We
# start by creating instances of the classes that will be doing the grunt of the
# work.
data = new DataModel text: defaultData
dataForm = new DataForm model: data
templates = new Collection defaultTemplates
templateForms = new TemplateCollectionView collection: templates
partials = new PartialManager templates

# Add a request handler to take care of user template rendering.
channel.reply 'render', (templateModel) ->
    view = data.eval()
    templateFn = mustache templateModel.get 'text'
    templateFn view, partials

# Ensure that meaningful feedback is shown when a forbidden word prevents
# rendering.
data.on 'invalid', (model, match) ->
    word = match[0]
    start = match.index
    alert "
        Alright, we lied a little. You cannot just enter *any* JavaScript
        value, because that might enable someone to trick another user into (for
        example) sending their cookies to a remote API. For this reason, we do
        not allow words like \"#{word}\".

        \n\nPlease edit your input data so it no longer looks suspicious to our
        (admittedly over-cautious) validator. For your convenience, we have
        already selected the detected word.
    "
    window.getSelection().removeAllRanges()
    dataForm.$ 'textarea'
    .focus().get 0
    .setSelectionRange start, start + word.length

# Define event handlers for load/store functionality.
store = (event) ->
    event.originalEvent.clipboardData
    .setData 'text/plain', JSON.stringify {data, templates}
    event.preventDefault()

load = (event) ->
    event.preventDefault()
    clipboard = event.originalEvent.clipboardData or window.clipboardData
    try
        json = JSON.parse clipboard.getData 'text'
    catch
    unless (isObject(json) and isObject(json.data) and isArray(json.templates))
        alert 'Your clipboard does not seem to contain a saved playground
            session. Please try copy-pasting again.'
    else
        data.set json.data
        templates.set json.templates
        dataForm.editor.render().activate()
        # Trigger validation at the end, so the user can see the trigger word.
        data.isValid()

# Finally, when the DOM has loaded,
$ ->
    # we reveal the views to the user,
    $('main').append dataForm.el, templateForms.el
    # resizing the text areas to fit the default content,
    dataForm.activate()
    templateForms.activate()
    # and bind the necessary events to enable load/store.
    $('#loadstore').on copy: store, paste: load
