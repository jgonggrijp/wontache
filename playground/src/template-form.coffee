import { defer } from 'underscore'
import { $ } from 'backbone'
import { CompositeView } from 'backbone-fractal'

import channel from './radio.coffee'
import CodeEditor from './code-field-view.coffee'
import templateFormTemplate from './template-form.mustache'

# The elements with this selector are dynamically shown and hidden.
dynamics = 'button[type="reset"], pre'

# As a visual help to playground users, we number the templates sequentially.
# This shared counter will be incremented.
templateCount = 1

# Wrapper for event listeners that need to call `event.preventDefault()`.
preventDefault = (f) ->
    (event) ->
        event.preventDefault()
        f.call @, event

# A view that lets the user enter a single template, together with an optional
# name. It has buttons to show how the template will render, to clear the
# output, and to delete the template.
export default class TemplateForm extends CompositeView
    tagName: 'form'
    template: templateFormTemplate
    subviews: [
        {view: 'editor', selector: 'div', method: 'after'}
    ]
    events:
        'submit': 'renderUserTemplate'
        'reset': 'clearOutput'
        'click button.danger': 'deleteUserTemplate'
        'change input': 'updateName'

    initialize: ->
        @index = templateCount++
        @editor = new CodeEditor model: @model
        @render().hideDynamics()

    renderContainer: ->
        @$el.html @template
            cid: @cid
            index: @index
            name: @model.get 'name'
        @

    activate: ->
        unless @active
            @active = true
            @forEachSubview (view) -> view.activate()
        @

    hideDynamics: ->
        @$(dynamics).hide()
        @

    showDynamics: ->
        elements = @$ dynamics
        .show()
        code = elements.eq 1
        height = code.outerHeight()
        offsetTop = code.offset().top
        page = $ window
        estate = page.height()
        scrollPosition = page.scrollTop()
        if scrollPosition + estate - offsetTop < 10
            target = if height < estate
                offsetTop + height - estate
            else
                elements.offset().top
            limit = scrollPosition + estate / 2
            $('html, body').animate scrollTop: Math.min target, limit
        @

    renderUserTemplate: preventDefault ->
        button = @$ 'button[type="submit"]'
        .html '<em>Rendering...</em>'
        # We perform the remainder of this handler async, to ensure that the
        # above change is immediately visible to the user. We could also insert
        # an `await null` to create an artificial barrier, but modern browsers
        # will actually await the promise before rendering anything.
        defer =>
            target = @$ 'code'
            # Rather than trying to collect all the data needed to render the
            # template right here in the view, we simply signal the need over
            # the radio channel and hope that something is listening at the
            # other end. That's the power of double dispatch!
            try
                target.text channel.request 'render', @model
            catch error
                target.html "⚠️ <b>#{error}</b>"
            @showDynamics()
            button.text 'Render'

    clearOutput: preventDefault ->
        @hideDynamics().$('code').text ''

    deleteUserTemplate: ->
        @model.destroy()

    updateName: ->
        @model.set 'name', @$('input').val()
