import assert from 'assert'
import sinon from 'sinon'

import { Collection } from 'backbone'

import TemplateForm from './template-form.coffee'
import TemplateCollectionView from './template-collection-view.coffee'

describe 'TemplateCollectionView', ->
    beforeEach ->
        @collection = new Collection [{}]
        @view = new TemplateCollectionView collection: @collection

    afterEach ->
        sinon.restore()
        @collection.reset()
        @view.remove()

    it 'renders on construction', ->
        assert @view.$el.html()

    it 'initializes its subviews', ->
        assert @view.items.length is 1
        assert @view.items[0] instanceof TemplateForm

    it 'enables the default collection event listeners', ->
        @collection.add {}
        assert @view.items.length is 2
        @collection.reset()
        assert @view.items.length is 0

    it 'has a button of its own', ->
        assert @view.$('button').length is 4
        @collection.reset()
        assert @view.$('button').length is 1

    it 'activates its subviews', ->
        sinon.replace @view.items[0], 'activate', sinon.fake()
        @view.activate()
        assert @view.items[0].activate.calledOnce

    it 'activates newly added subviews post-activation', ->
        watch = sinon.fake()
        sinon.replace TemplateForm.prototype, 'activate', watch
        @collection.add {}
        assert watch.notCalled
        @view.activate()
        assert watch.callCount is 2
        @collection.add {}
        assert watch.callCount > 2

    it 'adds another template when its button is clicked', ->
        button = @view.$el.children 'button'
        assert button.length is 1
        button.click()
        assert @view.items.length is 2

    it 'does not listen to its subview\'s buttons', ->
        @view.items[0].$('button[type="submit"]').click()
        assert @view.items.length is 1
