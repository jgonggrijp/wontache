import assert from 'assert'
import sinon from 'sinon'

import { constant, defer } from 'underscore'
import { Model } from 'backbone'

import channel from './radio.coffee'
import CodeEditor from './code-field-view.coffee'
import TemplateForm from './template-form.coffee'

# The exact same spec appears in two distinct suites, so we factored it out
# here.
expectHidden = ->
    assert @view.$('button[type="reset"]').css('display') is 'none'
    assert @view.$('pre').css('display') is 'none'

describe 'TemplateForm', ->
    beforeEach ->
        @model = new Model name: 'Henry'
        @view = new TemplateForm model: @model

    afterEach ->
        @view.remove()

    it 'receives increasing indices', ->
        view2 = new TemplateForm model: @model
        assert view2.index - @view.index is 1
        view2.remove()

    it 'creates an editor subview on initialize', ->
        assert @view.editor instanceof CodeEditor

    it 'renders on initialize', ->
        assert @view.$el.html()

    it 'includes a previously set template name in the render', ->
        assert /Henry/.test @view.$el.html()

    it 'hides the clear button and output preview initially', expectHidden

    describe '.activate', ->
        beforeEach ->
            @subview = @view.editor
            sinon.replace @subview, 'activate', sinon.fake()
            @view.activate()

        afterEach ->
            sinon.restore()

        it 'activates the editor subview', ->
            assert @subview.activate.calledOnce

        it 'is idempotent', ->
            @view.activate()
            assert @subview.activate.calledOnce

    describe '.renderUserTemplate', ->
        beforeEach (done) ->
            channel.reply 'render', constant 'abcdef'
            @view.$('button[type="submit"]').trigger('submit')
            defer done

        afterEach ->
            channel.stopReplying()

        it 'reveals the clear button and output preview', ->
            assert @view.$('button[type="reset"]').css('display') isnt 'none'
            assert @view.$('pre').css('display') isnt 'none'

        it 'issues a "render" request to the channel and shows the reply', ->
            assert @view.$('code').text() is 'abcdef'

        it 'displays meaningful feedback on failure', (done) ->
            channel.reply 'render', -> throw new Error 'oh dear'
            @view.$('button[type="submit"]').trigger('submit')
            defer =>
                assert /oh dear/.test @view.$('code').text()
                done()

        # The next suite normally would be one indentation level lower, as the
        # named method belongs to `TemplateForm` and not to
        # `renderUserTemplate`. However, clearing the output is typically
        # something you do after rendering the template, so in this case it is
        # convenient to chain the fixture setups.
        describe '.clearOutput', ->
            beforeEach ->
                @view.$('button[type="reset"]').trigger('reset')

            it 'erases the output preview', ->
                assert @view.$('code').text() is ''

            it 'hides the clear button and output preview', expectHidden

    describe '.deleteUserTemplate', ->
        beforeEach ->
            @watch = sinon.fake()
            @model.on 'destroy', @watch
            @view.$('button.danger').click()

        afterEach ->
            @model.off()

        it 'destroys the model', ->
            assert @watch.calledOnce

    describe '.updateName', ->
        beforeEach ->
            @view.$('input').val('Lucy').trigger 'change'

        it 'updates the model', ->
            assert @model.get('name') is 'Lucy'
