import { bind, extend } from 'underscore'
import { Events } from 'backbone'

# Given events from a collection of templates, maintain the administration that
# Wontache-compiled templates require in order to find their partials.
# `collection` should be a collection of models with `'text'` and optional
# `'name'` attributes. Models without a `'name'` will be silently ignored. The
# administration can be read out at any time through the `partials` property.
export default class PartialManager
    constructor: (@collection) ->
        @partials = {}
        @collection.each bind @addTemplate, @
        @listenTo @collection,
            'add': @addTemplate
            'remove': @removeTemplate
            'change:name': @renameTemplate
            'change:text': @changeTemplate

    inject: (name, text) ->
        if name then @partials[name] = text

    eject: (name) ->
        if name then delete @partials[name]

    addTemplate: (template) ->
        @inject template.get('name'), template.get('text')

    removeTemplate: (template) ->
        @eject template.get 'name'

    renameTemplate: (template, newName) ->
        @eject template.previous 'name'
        @inject newName, template.get 'text'

    changeTemplate: (template, newText) ->
        @inject template.get('name'), newText

extend PartialManager.prototype, Events
