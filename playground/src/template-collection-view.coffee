import { invoke } from 'underscore'
import { CollectionView } from 'backbone-fractal'

import TemplateForm from './template-form.coffee'
import templateCollectionTemplate from './template-collection-view.mustache'

# Show an array of template forms, with the option to add more with a button.
# Each template form has a button of its own so it can also be removed.
export default class TemplateCollectionView extends CollectionView
    template: templateCollectionTemplate
    subview: TemplateForm
    container: 'div'
    events:
        'click button.more': 'addTemplate'

    initialize: ->
        @initItems().render().initCollectionEvents()

    renderContainer: ->
        @$el.html @template()
        @

    activate: ->
        @active = true
        invoke @items, 'activate'
        @

    placeItems: ->
        super.placeItems()
        if @active then @activate() else @

    addTemplate: ->
        @collection.add {}
