import assert from 'assert'

import { size } from 'underscore'
import { Collection } from 'backbone'

import PartialManager from './partial-manager.coffee'

initialData = [{
    name: 'banana'
    text: 'bananas'
}, {
    text: 'cherries'
}]

describe 'PartialManager', ->
    beforeEach ->
        @collection = new Collection initialData
        @manager = new PartialManager @collection
        @partials = @manager.partials

    afterEach ->
        @manager.off()
        @collection.off().reset()

    it 'initializes with all named models in the collection', ->
        assert size(@partials) is 1
        assert @partials.banana is 'bananas'

    it 'updates when named templates are added', ->
        @collection.add name: 'date', text: 'dates'
        assert size(@partials) is 2
        assert @partials.date is 'dates'
        assert 'banana' of @partials

    it 'ignores the addition of unnamed templates', ->
        @collection.add text: 'dates'
        assert size(@partials) is 1
        assert 'banana' of @partials

    it 'updates when named templates are removed', ->
        assert @collection.shift().get('name') is 'banana'
        assert size(@partials) is 0

    it 'ignores the removal of unnamed templates', ->
        assert not @collection.pop().has 'name'
        assert size(@partials) is 1
        assert 'banana' of @partials

    it 'adds pre-existing templates when they obtain a name', ->
        last = @collection.last()
        assert not last.has 'name'
        last.set 'name', 'cherry'
        assert size(@partials) is 2
        assert 'banana' of @partials
        assert @partials.cherry is 'cherries'

    it 'updates when pre-existing templates change name', ->
        first = @collection.first()
        assert first.get('name') is 'banana'
        first.set 'name', 'BANANA'
        assert size(@partials) is 1
        assert @partials.BANANA is 'bananas'

    it 'removes templates that lost their name', ->
        first = @collection.first()
        first.unset 'name'
        assert size(@partials) is 0

    it 'updates the text of pre-existing templates', ->
        first = @collection.first()
        first.set 'text', 'more bananas'
        assert size(@partials) is 1
        assert @partials.banana is 'more bananas'

    it 'ignores text changes in unnamed templates', ->
        last = @collection.last()
        last.set 'text', 'more cherries'
        assert size(@partials) is 1
        assert @partials.banana is 'bananas'

    it 'remains consistent if a template changes both attributes', ->
        first = @collection.first()
        first.set name: 'almond', text: 'almonds'
        assert size(@partials) is 1
        assert @partials.almond is 'almonds'
