# Give visitors something to work with, and provide a quick cheatsheet to those
# not familiar with JavaScript.
export defaultData = '''
    /* comments between slashes and stars */
    {
        /* objects/hashes/dictionaries between curly braces */
        number: 1.5,
        boolean: true,
        string: 'hello',
        nested: {
            string: 'world', /* trailing commas allowed */
        },
        list: [null, 'one', 2],
        emphasize: function(section) {
            /* lambdas */
            return '<em>' + section + '</em>';
        },
    }
'''

# Again, give visitors something to work with. Quick demonstration of variables,
# sections, dotted names, lambdas, implicit iterator and partials.
export defaultTemplates = [{
    name: 'example1'
    text: '''
        {{string}} {{#emphasize}}{{nested.string}}{{/emphasize}}
        <ul>
            {{#list}}
            <li>{{.}}
            {{/list}}
        </ul>

    '''
}, {
    name: 'example2'
    text: '''
        Here is a quick trick to indent the previous template:
            {{>example1}}

    '''
}]
