import { View } from 'backbone'

import editorTemplate from './code-field-view.mustache'

# Multiline editor in which the user can enter JavaScript or Mustache code.
export default class CodeFieldView extends View
    tagName: 'fieldset'
    className: 'won-editor'
    template: editorTemplate
    events:
        change: -> @model.set 'text', @$('textarea').val(), validate: true

    initialize: ->
        @render()

    render: ->
        @$el.html @template
            text: @model.get 'text'
            cid: @cid
        @

    activate: ->
        area = @$ 'textarea'
        area.height area.prop 'scrollHeight'
        @
