import assert from 'assert'
import sinon from 'sinon'

import DataModel from './data-model.coffee'
import CodeEditor from './code-field-view.coffee'
import DataForm from './data-form.coffee'

describe 'DataFormView', ->
    beforeEach ->
        @model = new DataModel
        @view = new DataForm model: @model
        @subview = @view.editor

    afterEach ->
        sinon.restore()
        @view.remove()

    it 'creates an editor subview with the same model', ->
        assert @subview instanceof CodeEditor
        assert @subview.model is @view.model

    it 'self-renders', ->
        assert @view.$el.text()
        assert @view.$('.won-editor').get(0) is @subview.el

    it 'activates the editor subview', ->
        sinon.replace @subview, 'activate', sinon.fake()
        @view.activate()
        assert @subview.activate.calledOnce
