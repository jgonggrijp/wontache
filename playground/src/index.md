# Mustache playground

Below, you can define input data and one or more [Mustache][mustache] templates. Pressing the "Render" button below a template reveals the result of that template, given your input.

You can use any single JavaScript expression as input, including nested objects and methods. Naming templates is **optional**. Doing so enables you to interpolate the named template inside other templates using `{{>partial}}` or `{{<parent}}{{/parent}}` tags. (You can also interpolate a template inside itself, but you must be careful to prevent infinite recursion in that case.)

You can save the entire session in textual form to your clipboard by clicking/tapping in the box below and using your operating system's "copy" feature (ctrl-c, cmd-c, etcetera). You can also restore an existing savestate by pasting into the box.<br>
<input id=loadstore placeholder="load/store"><br>
*Tip: template numbering might change after a store-load cycle. Use names to refer to individual templates.*

This playground is backed by version 0.2.0 of the [Wontache][wontache] engine, which fully passes version 1.4.2 of the [Mustache specification][spec], including all optional modules. This means that you can use all template syntax described in the [updated `mustache(5)` manpage][man5]. The source code of the playground is [here][source].

[mustache]: http://mustache.github.io/
[wontache]: https://jgonggrijp.gitlab.io/wontache/index.html
[spec]: https://github.com/mustache/spec
[man5]: https://jgonggrijp.gitlab.io/wontache/mustache.5.html
[source]: https://gitlab.com/jgonggrijp/wontache/-/tree/integration/playground

<main></main>
<script src="./playground.js"></script>
