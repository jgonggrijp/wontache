import assert from 'assert'
import sinon from 'sinon'
import { times, constant } from 'underscore'

import DataModel from './data-model.coffee'
import CodeFieldView from './code-field-view.coffee'

longText = times 20, constant 'abcdef ghi jklm nopq rstuv wxyz'
.join '\n'

describe 'CodeFieldView', ->
    beforeEach ->
        @model = new DataModel text: '"abcdef"'
        @view = new CodeFieldView model: @model

    afterEach ->
        @view.remove()

    it 'auto-renders on construction', ->
        id = "editor-#{@view.cid}"
        label = @view.$ 'label'
        assert label.length is 1
        assert label.attr('for') is id
        assert label.text()
        textarea = @view.$ 'textarea'
        assert textarea.length is 1
        assert textarea.attr('id') is id
        assert /^"abcdef"$/.test textarea.html()

    it 'updates the model on user edit', ->
        watch = sinon.fake()
        @model.on 'change:text', watch
        @view.$('textarea').val('"xyz"').trigger('change')
        assert @model.get('text') is '"xyz"'
        assert watch.calledOnce

    it 'validates the user input', ->
        watchChange = sinon.fake()
        watchInvalid = sinon.fake()
        @model.on 'change:text', watchChange
        @model.on 'invalid', watchInvalid
        @view.$('textarea').val('localStorage').trigger('change')
        assert not watchChange.called
        assert watchInvalid.calledOnce
        assert @model.get('text') is '"abcdef"'

    it 'scales the <textarea> to fit the content', ->
        @model.set 'text', longText
        @view.$el.appendTo document.body
        @view.render().activate()
        area = @view.$ 'textarea'
        height = area.innerHeight()
        scrollHeight = area.prop 'scrollHeight'
        assert height is scrollHeight
