import { Model } from 'backbone'

# Words we don't want to encounter in data expressions, because they could be
# used to hijack somebody's browser.
suspicious = ///
    window|
    global|
    self|
    document|
    sessionStorage|
    localStorage|
    history|
    location|
    fetch|
    XMLHttpRequest|
    ajax|
    decodeURI|
    __proto__|
    prototype|
    constructor|
    eval|
    Function
///

# This model, or rather its `text` attribute, represents the "view" (Mustache
# jargon) that is passed as data argument into a template.
export default class DataModel extends Model
    # The `text` attribute can contain arbitrary JavaScript code. We validate
    # that attribute to rule out that people try to do funky things with the
    # browser.
    validate: (attributes) ->
        attributes?.text?.match? suspicious

    # When actually using the data view, it should be an actual JavaScript value
    # rather than text enconding such a value. This method does that; it is
    # nearly as dangerous as the name suggests.
    eval: ->
        if @isValid()
            Function("return (#{@get 'text'})")()
