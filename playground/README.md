# @wontache/playground

*Online playground for trying out [Mustache][mustache] templates, powered by the [Wontache][wontache] engine*

This is the source code for the [online Mustache playground][playground] that is part of the Wontache project. Currently, it is not published to NPM. For usage, see the text at the top of the playground page.

[mustache]: http://mustache.github.io
[wontache]: https://jgonggrijp.gitlab.io/wontache/index.html
[playground]: https://jgonggrijp.gitlab.io/wontache/playground.html
