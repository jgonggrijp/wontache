# Contributing to @wontache/playground

The playground is a small client side web application based on [Backbone][backbone] and (of course!) Wontache.

[backbone]: https://backbonejs.org


## Development commands

```sh
yarn playground-html # note the dash!
```

Will compile the HTML container of the playground. You need to run this command manually whenever you change `src/index.md`. Do not commit unless you are preparing a release.

```sh
yarn playground jquery
```

Creates a custom build of jquery that we depend on. Make sure you have initialized the `./jquery` submodule before running this command, and make sure you run this command before trying any of the commands below.

```sh
yarn playground serve
```

Starts a development server that lets you try out the current code in a web browser at http://localhost:8080/playground.html. It automatically rebuilds when you edit CoffeeScript or Mustache source files, but you need to refresh the browser manually in order to actually see the changes.

```sh
yarn playground test
```

Runs a test server using Karma. Open http://localhost:9876/ in a browser to run the tests. As long as you keep the browser tab open, tests will automatically run again when you edit the source code. Depending on your system, you may need to keep the tab visible for the best performance.

```sh
yarn playground bundle
```

Outputs the optimized JavaScript bundle to `../public`. The bundle is intended for end users, but you may want to check that it is correct. Do not commit unless you are preparing a release.
