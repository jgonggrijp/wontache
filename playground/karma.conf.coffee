# Karma configuration
# Generated on Fri Sep 02 2022 17:00:41 GMT+0200 (Central European Summer Time)

module.exports = (config) ->
    config.set
        # base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: ''

        # frameworks to use
        # available frameworks: https://www.npmjs.com/search?q=keywords:karma-adapter
        frameworks: ['mocha', 'power-assert']

        # list of files / patterns to load in the browser
        files: [
            'src/test-index.coffee'
        ]

        # list of files / patterns to exclude
        exclude: [
        ]

        # preprocess matching files before serving them to the browser
        # available preprocessors: https://www.npmjs.com/search?q=keywords:karma-preprocessor
        preprocessors:
            'src/test-index.coffee': ['rollup']

        rollupPreprocessor:
            plugins: [
                require('rollup-plugin-node-polyfills')()
                require('rollup-plugin-coffee-script')()
                require('rollup-plugin-wontache')({
                    precompile: true
                })
                require('rollup-plugin-glob-import')({
                    format: 'import'
                })
                require('@rollup/plugin-json')()
                require('@rollup/plugin-alias')
                    entries: [
                        find: /.*jquery.*/
                        replacement: './jquery/dist/jquery.js'
                    ]
                require('@rollup/plugin-node-resolve').default({
                    preferBuiltins: true
                    dedupe: ['underscore']
                })
                require('@rollup/plugin-commonjs')()
                require('@rollup/plugin-inject')({
                    'window._': 'underscore'
                    'window.Backbone': 'backbone'
                    include: 'node_modules/backbone-fractal/**/*.js'
                    include: '../node_modules/backbone-fractal/**/*.js'
                })
                require('@rollup/plugin-babel').default({
                    extensions: ['.coffee']
                    presets: ['power-assert']
                    babelHelpers: 'bundled'
                })
            ]
            output:
                format: 'iife'
                name: 'test'
                sourcemap: 'inline'

        # test results reporter to use
        # possible values: 'dots', 'progress'
        # available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
        reporters: ['progress']

        # web server port
        port: 9876

        # enable / disable colors in the output (reporters and logs)
        colors: true

        # level of logging
        # possible values:
        # - config.LOG_DISABLE
        # - config.LOG_ERROR
        # - config.LOG_WARN
        # - config.LOG_INFO
        # - config.LOG_DEBUG
        logLevel: config.LOG_INFO

        # enable / disable watching file and executing tests whenever any file changes
        autoWatch: true

        # start these browsers
        # available browser launchers: https://www.npmjs.com/search?q=keywords:karma-launcher
        browsers: []

        # Continuous Integration mode
        # if true, Karma captures browsers, runs the tests and exits
        singleRun: false

        # Concurrency level
        # how many browser instances should be started simultaneously
        concurrency: Infinity
