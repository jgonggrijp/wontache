# Contributing to the Wontache project

## How the monorepo works

- All JavaScript source code is written in [CoffeeScript][coffee]. However, all code is transpiled to plain JavaScript before being published.
- We use the [git-flow][gitflow] branching model with modified names: `integration` is where we merge feature branches, `production` is the branch that consists purely of official releases.
- We use classic `yarn` as the package manager. Please keep the `yarn.lock` in sync with the `package.json` files and do not commit a `package-lock.json` or other files specific to the `npm` command.
- We use [yarn workspaces][workspaces] so that packages can internally depend on each other for development purposes, even though some of them are not published to NPM. In order to add such an internal-only dependency, manually edit the `package.json` of the dependent package rather than using `yarn add`. Remember to only do this with `devDependencies`.
- We use [GitLab CI/CD][gitlab-ci] to run continuous integration tests and to update the website. See the [`.gitlab-ci.yml`][ci-yml] for the details.
- We use `mocha` with `power-assert` for unittesting.

[coffee]: https://coffeescript.org/
[gitflow]: https://nvie.com/posts/a-successful-git-branching-model/
[workspaces]: https://classic.yarnpkg.com/en/docs/workspaces
[gitlab-ci]: https://docs.gitlab.com/ee/ci/pipelines/
[ci-yml]: https://gitlab.com/jgonggrijp/wontache/-/blob/integration/.gitlab-ci.yml


## Development commands

### `yarn`

You need to run `yarn` after first cloning the repository, as well as after switching to another branch or pulling in changes from a remote repository.

### `git submodule update --init [SUBMODULE]`

You need to initialize the `mustache/spec` submodule in order to be able to run the tests. If you would like to work on the `mustache(5)` manpage from within the monorepo, you can also initialize the `ruby-mustache` submodule. Initialize `playground/jquery` in order to develop the playground. Leave off the `[SUBMODULE]` argument to initialize all submodules.

### `git submodule update`

Like `yarn`, run this command after switching branches or pulling in changes from a remote repository.

### `yarn WORKSPACE COMMAND [ARGS...]`

By convention, we have scripts in the root `package.json` that let us abbreviate

    yarn workspace PACKAGE-NAME COMMAND [ARGS...]

to the above, where `WORKSPACE` is the *directory* name of a workspace, i.e., `comparison`, `mustache` or `tool-*` (the latter replaces `tools/*`). This lets you execute regular `yarn` commands within the context of one of the workspaces ([docs][workspace-cli]).

A `COMMAND` that you'll likely run often is `test`. You can also run `test inspect` to step through the tests with a debugger. Probably most useful if you set a breakpoint in advance. You might want to disable some lengthy tests that you're not interested in as well.

Another `COMMAND` that frequently appears is `runcoffee`, which lets us execute a CoffeeScript file with ESM `import`/`export` notation without having to transpile it first. This command is implemented in our own `tools/runcoffee` workspace. Due to current limitations in Node.js, this command ironically only works in directories governed by a `package.json` which has `"type": "commonjs"`.

[workspace-cli]: https://classic.yarnpkg.com/en/docs/cli/workspace


## Formatting code in READMEs

The READMEs in rendered form are used as website pages. To ensure that the code examples look good, insert a blank line after the

    ```js

code fence and restrict code line length to 60 characters where possible.


## Shebang lines (`#!/usr/bin/env node`) in transpiled CoffeeScript

... are not straightforward to add. You need to compile to JavaScript using `coffee -c --no-header` and use the following notation at the top of the CoffeeScript source:

    ```#!/usr/bin/env node
    ```
