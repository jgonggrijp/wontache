import coffee from 'rollup-plugin-coffee-script'
import { bundleInput } from './index.coffee'

bundleInput
    input: 'index.coffee'
    external: /./
    plugins: [
        coffee()
    ]
    output:
        file: 'index.js'
        format: 'cjs'
        sourcemap: true
.catch console.error
