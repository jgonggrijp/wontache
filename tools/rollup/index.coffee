import { isArray } from 'underscore'
import { each, asyncify } from 'async'
import { rollup } from 'rollup'

# Bundle and write one input config with outputs. Inspiration taken from
# https://rollupjs.org/guide/en/#programmatically-loading-a-config-file.
export bundleInput = (inputConf) ->
    bundle = await rollup inputConf
    outputConf = inputConf.output
    if not isArray outputConf then outputConf = [outputConf]
    await each outputConf, asyncify bundle.write
    bundle.close()

# Asyncified version for consumers that strictly rely on the callback convention
# (for example when passed to async.apply).
export bundleInputA = asyncify bundleInput
