# Wontache development tools

*Development tools for use within the Wontache monorepo*

This is a directory with private [workspaces][workspaces] within the [Wontache][wontache] project. As the name implies, it provides tools for the development of Wontache.

Tools listed below. Unless noted otherwise, they can be run with a `--help` flag for usage details.

[wontache]: https://gitlab.com/jgonggrijp/wontache
[workspaces]: https://classic.yarnpkg.com/en/docs/workspaces


## `runcoffee`

Lets us run CoffeeScript files with ESM `import`/`export` notation in Node.js, without having to transpile them first in a separate step. Node.js does not support this out of the box yet, so we fake it for the time being, by having `babel` transpile the ESM notation to CommonJS notation. Has a `debug` option in case you want to run the script with the debugger enabled.

This tool uses a `babel.coffee` register module under the hood. This also enables `power-assert`. This can also be used directly as `wontache-devtools-runcoffee/babel.coffee`, provided that `wontache-devtools-runcoffee` is a `devDependency` of the package in which it is used.


## `wrap-readme`

Part of continuous deployment, so usually you don't need to run it manually. Takes a Markdown file, renders it to HTML markup and wraps the markup in a full HTML document that references our own stylesheet and scripts.
