#!/usr/bin/env runcoffee

import assert from 'assert'
import { readFileSync, writeFileSync } from 'fs'
import * as path from 'path'

import { mapObject, partial, identity, find } from 'underscore'
import mustache from 'wontache'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

# The `semver` regex does not support build metadata because we do not want
# those to appear in our version history.
bareSemver = /((\d+)\.(\d+)\.(\d+))(?:-([.0-9A-Za-z-]+))?/
semver = new RegExp "^#{bareSemver.source}$"
oldPackageVersion = new RegExp "^- +\"version\": \"(#{bareSemver.source})\"", 'm'

# Workspaces that aren't published to NPM but still have a web page.
privateWithSite = ['playground']

templates =
    staticallyRoot: 'https://cdn.statically.io/gl/jgonggrijp/wontache'
    staticallyUrl: '{{>staticallyRoot}}/{{&tagName}}/public/{{&sitePage}}.html'
    packageUrl: 'https://www.npmjs.com/package/{{&pkgName}}/v/{{&version}}'
    diffRoot: 'https://gitlab.com/jgonggrijp/wontache/-/compare'
    diffUrl: '{{>diffRoot}}/{{&oldTagName}}...{{&tagName}}'

    tagScript: mustache '''
        {{#workspaces}}
        git tag -m "{{&tagMessage}}" {{&tagName}}
        {{/workspaces}}
    '''

    gitlabApiCall: mustache '''
        curl --header 'Content-Type: application/json' {{!
          }} --header "PRIVATE-TOKEN: $GITLAB_PUSH_TOKEN" {{!
          }} --data '{
              "name": "{{$namePattern}}{{&pkgName
                  }} {{&version}}{{/namePattern}}",
              "tag_name": "{{&tagName}}",
              {{#isMain}}
              "description": {{&changes}},
              {{/isMain}}
              {{#hasSite}}
              "assets": {
                  "links": [{
                      "name": "{{&siteTitle}} on statically.io",
                      "url": "{{>staticallyUrl}}"
                  {{#isPublic}}
                  }, {
                      "name": "{{&pkgName}}@{{&version}} on npm",
                      "url": "{{>packageUrl}}",
                      "link_type": "package"
                  {{/isPublic}}
                  {{#isMain}}
                  }, {
                      "name": "diff",
                      "url": "{{>diffUrl}}"
                  {{/isMain}}
                  }]
              }
              {{/hasSite}}
          }'

    '''

    gitlabScript: mustache '''
        {{#main}}
        {{<gitlabApiCall}}
            {{$namePattern}}monorepo (whole project) {{&version}}{{/namePattern
            }}
        {{/gitlabApiCall}}
        {{/main}}
        {{#workspaces}}
        {{^isMain}}
        {{<gitlabApiCall}}{{/gitlabApiCall}}
        {{/isMain}}
        {{/workspaces}}
    '''

mustache.partials = templates

namePackage = (directory) ->
    isCore = directory is 'mustache'
    nameParts = directory.split path.sep
    if nameParts[0] in ['plugins', 'tools']
        nameParts[0] = nameParts[0].slice 0, -1
        tagMessageName = "#{nameParts.slice(1).join '-'} #{nameParts[0]}"
    workspace = nameParts.join '-'
    shortName = if isCore then 'wontache' else workspace
    isTool = nameParts[0] is 'tool'
    tagMessageName ?= shortName or 'whole project'
    if isCore then tagMessageName = "core #{tagMessageName}"
    sitePage = (not isCore and shortName or 'index')
    {isCore, isTool, workspace, shortName, tagMessageName, sitePage}

parsePackageJson = (packageJson) ->
    {
        version, private: isPrivate, name: pkgName
    } = require path.resolve packageJson
    isPublic = !isPrivate
    {pkgName, version, isPublic}

parseDiff = (packageJson) ->
    jsonDiff = readFileSync "#{packageJson}.diff", 'utf-8'
    oldPackageVersion.exec(jsonDiff)?[1]

nameTags = (shortName, isMain, version, oldVersion) ->
    mapObject {tagName: version, oldTagName: oldVersion}, if isMain
        identity
    else
        (version) -> "#{shortName}-#{version}"

parseVersion = (version, hotfix) ->
    [, bare, major, minor, patch, pre] = semver.exec version
    releaseType = switch
        when bare is '0.1.0' then 'initial'
        when patch isnt '0'
            if hotfix then 'hotfix' else 'bugfix'
        when minor isnt '0' then 'feature'
        when major is '1' then 'first official'
        else 'major'
    {releaseType, bare, pre}

capitalize = (string) -> string[0].toUpperCase() + string.slice 1

describeRelease = (releaseType, bare, pre, isMain, tagMessageName) ->
    capitalize switch
        when pre then "prerelease of #{tagMessageName} #{bare} (#{releaseType})"
        when isMain then "whole-project #{releaseType} release"
        else "#{releaseType} release of #{tagMessageName}"

describeSite = (isMain, isPublic, sitePage, version) -> switch
    when isMain then 'project website'
    when isPublic then 'documentation'
    else "#{sitePage}@#{version}"

getWorkspaceInfo = (hotfix, packageJson) ->
    {dir, base} = path.parse packageJson
    assert base is 'package.json'
    isMain = !dir
    {
        isCore, isTool, workspace, shortName, tagMessageName, sitePage
    } = namePackage dir
    assert isMain or shortName
    {pkgName, version, isPublic} = parsePackageJson packageJson
    oldVersion = parseDiff packageJson
    {tagName, oldTagName} = nameTags shortName, isMain, version, oldVersion
    assert isMain or pkgName
    {releaseType, bare, pre} = parseVersion version, hotfix
    tagMessage = describeRelease releaseType, bare, pre, isMain, tagMessageName
    siteTitle = describeSite isMain, isPublic, sitePage, version
    hasSite = (isMain or isPublic or (workspace in privateWithSite))
    {
        isMain, isCore, isPublic, isTool, workspace, pkgName, hasSite, sitePage
        version, pre, tagName, tagMessage, oldVersion, oldTagName, siteTitle
    }

# The main routine of this tool: write two shell scripts, one to
# tag the releases and one to register them with the GitLab API.
main = (argv) ->
    changes = JSON.stringify readFileSync argv.changes, 'utf8'
    .replace /'/g, ''
    manifest = readFileSync argv.manifest, 'utf8'
    workspaces = manifest.trim().split '\n'
    .map partial getWorkspaceInfo, argv.hotfix
    mainSpace = find workspaces, isMain: true
    data = {changes, workspaces, main: mainSpace}
    writeFileSync argv.tag, templates.tagScript data
    writeFileSync argv.release, templates.gitlabScript data

main.description = 'Write shell scripts to facilitate continuous delivery.'

yargs hideBin process.argv
.command '$0', main.description, {}, main
.option 'c',
    alias: 'changes'
    description: 'git listing of changes on develop since previous release'
    normalize: true
    type: 'string'
.option 'm',
    alias: 'manifest'
    description: 'list of package.json files with changed version numbers'
    normalize: true
    type: 'string'
.option 'f',
    alias: 'hotfix'
    description: 'indicates that this release is a hotfix'
    type: 'boolean'
.option 't',
    alias: 'tag'
    description: 'destination file path for the tag creation script'
    normalize: true
    type: 'string'
.option 'r',
    alias: 'release'
    description: 'destination file path for the release publication script'
.help()
.version()
.alias 'help', 'h'
.alias 'version', 'v'
.parse()
