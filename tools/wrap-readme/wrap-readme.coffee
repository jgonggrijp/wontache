#!/usr/bin/env runcoffee

import { readFileSync, writeFileSync } from 'fs'

import { marked } from 'marked'
import mustache from 'wontache'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

# Prefix to our website. We strip it from URLs in our generated HTML to make
# them relative, so that links within local copies of the site stay local.
websitePrefix = /(?<=href=")https:\/\/jgonggrijp\.gitlab\.io\/wontache\/(?!")/g

# Delimiters that we are pretty sure will never occur in a compiled README.
exoticDelimiters = ['<˚))::-<', '>-::((˚>']

# Template that will wrap HTML output from `marked` into a fully fledged page,
# with syntax highlighting through prism.js.
render = mustache '''
    <!DOCTYPE html>
    <html><head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{title}}</title>
        <link rel=stylesheet href="index.css">
    </head><body>
        {{!
            The content is just a fixed string, but we turn it into a partial
            in order to correct indentation.
        }}
        {{>content}}
        <script src="prism.js"></script>
    </body></html>
'''

# The main routine of this tool: read a README.md, convert it to HTML, wrap it
# in the above page template and output it.
main = (argv) ->
    content = readFileSync argv.input, 'utf8'
    contentHtml = marked content, smartypants: true
    .replace websitePrefix, ''
    writeFileSync argv.output, render argv,
        partials:
            # As stated before in a template comment, we turn the HTML output
            # from `marked` into a partial so we get free reindentation.
            content: mustache contentHtml, exoticDelimiters

main.description = 'Render a README.md into an HTML page suitable for the
Wontache website.'

# Turn this into a nice tool by telling `yargs` what arguments we anticipate.
yargs hideBin process.argv
.command '$0', main.description, {}, main
.option 'i',
    alias: 'input'
    default: process.stdin.fd
    defaultDescription: 'stdin'
    description: 'Input README.md'
    normalize: true
    type: 'string'
.option 'o',
    alias: 'output'
    default: process.stdout.fd
    defaultDescription: 'stdout'
    description: 'Output html file'
    normalize: true
    type: 'string'
.option 't',
    alias: 'title'
    demandOption: true
    description: 'Browser window title'
    type: 'string'
.help()
.version()
.alias 'help', 'h'
.alias 'version', 'v'
.parse()
