require('@babel/register')
    extensions: ['.coffee']
    plugins: [
        '@babel/plugin-transform-modules-commonjs'
        ['module-resolver', alias:
            '^(underscore/)modules(/.*)': '\\1cjs\\2'
        ]
    ]
    presets: ['power-assert']
