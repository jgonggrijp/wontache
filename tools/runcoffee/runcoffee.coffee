#!/usr/bin/env coffee
foregroundChild = require 'foreground-child'
preloadList = require 'node-preload'
yargs = require 'yargs'
{ hideBin } = require 'yargs/helpers'

# The purpose of this tool is to make it easy to run CoffeeScript scripts with
# ES module notation in Node.js. We basically run `node` with two require hooks:

# Enable Node.js to read CoffeeScript.
preloadList.push require.resolve 'coffeescript/register'
# Transpile ESM notation to CommonJS notation. This is obviously a
# workaround; see https://nodejs.org/api/esm.html#loaders for why this is
# necessary and why we can't sustainably do it with "type": "module" yet.
# Also enables power-assert.
preloadList.push require.resolve 'wontache-devtools-runcoffee/babel.coffee'

# If we're just running the script, we'll call `node`; if we want to enable the
# debugger, we call `node inspect` instead.
nodeCommand = ['node']
inspectCommand = nodeCommand.concat 'inspect'

# Builder for the `yargs.command` method.
build = (yargs) ->
    yargs
    .positional 'script', description: 'CoffeeScript ES module path'
    .positional 'args', description: 'Arguments passed to the script'

# Package everything into a nice tool using yargs. We allow yargs to parse only
# the first argument; regardless of what it is, we want to forward the remaining
# arguments unintercepted to node.js. `.parse()` sets things in motion.
argv = yargs process.argv.slice 2, 3
.scriptName 'runcoffee'
.command '$0 [script] [args..]', 'Run CoffeeScript ES module', build, (argv) ->
    foregroundChild nodeCommand..., process.argv.slice(2)...
.command [
    'd [script] [args..]'
    'debug', 'inspect', 'i'
], 'Run CoffeeScript ES module with debugger', build, (argv) ->
    foregroundChild inspectCommand..., process.argv.slice(3)...
.alias 'help', 'h'
.alias 'version', 'v'
.parse()
